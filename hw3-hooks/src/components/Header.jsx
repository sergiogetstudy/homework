import React, { useContext } from "react";
import { FaHome, FaShoppingBag, FaShoppingCart, FaStar } from "react-icons/fa";
import { Link } from "react-router-dom";
import { ShoppingContext } from "../App";
import "./Header.scss";

export default function Header() {
  const { goodsOnCartId, favouriteGoods } = useContext(ShoppingContext);

  return (
    <header className="header">
      <Link className="link" to="">
        <div className="header__logo">
          <FaShoppingBag size={32} fill="white" className="header__logo-icon" />
          <FaShoppingBag size={32} fill="white" className="header__logo-icon--hover" />
          <h1 className="header__logo-name">useShopper</h1>
        </div>
      </Link>
      <div>
        <Link className="link" style={{display:"inline-block", width:"45px"}} to="">
          <FaHome size={26} style={{paddingRight:"10px"}} />
        </Link>
        <Link className="link" to="favourites">
          <div style={{display:"inline-block", width:"45px"}}>
            <FaStar size={26} />
            <span style={{paddingRight:"10px"}}>{favouriteGoods.length > 0 && favouriteGoods.length}</span>
          </div>
        </Link>
        <Link className="link" to="cart">
          <div style={{display:"inline-block", width:"45px"}}>
            <FaShoppingCart size={26} />
            <span>{goodsOnCartId.length > 0 && goodsOnCartId.length}</span>
          </div>
        </Link>
      </div>
    </header>
  )
}