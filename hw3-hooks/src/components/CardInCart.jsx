import React, { useContext } from "react";
import { ShoppingContext } from "../App";
import "./CartSection.scss";

export default function CordInCart(props) {
  const { name, price, imgPath, id, color } = props.item;
  const amount = props.amount;

  const { renderModal } = useContext(ShoppingContext);
  const uah = Intl.NumberFormat("ua", {style: "currency", currency: "UAH"});

  return (
    <>
      <span className="cart-grid__cell cell__center"><img className="cart-grid__img" alt={name} src={imgPath} /></span>
      <span className="cart-grid__cell"><h3>{name} {color}</h3></span>
      <span className="cart-grid__cell cell__center">{id}</span>
      <span className="cart-grid__cell cell__center">{amount}</span>
      <span className="cart-grid__cell cell__center">{uah.format(price)}</span>
      <span className="cart-grid__cell cell__center">{uah.format(price * amount)}</span>
      <span className="cart-grid__cell cell__center">
        <svg 
          style={{padding:"20px"}} 
          data-modalid="2" 
          data-itemid={id}
          onClick={(e) => {renderModal(e)}} 
          width="15" height="15" viewBox="0 0 26 27" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M1 1L23.5 24" stroke="black" strokeWidth="2" />
            <path d="M1 23L24 1" stroke="black" strokeWidth="2" />
        </svg>
      </span>
    </>
  )
}