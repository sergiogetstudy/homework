import React from "react";
import "./Modal.scss"
import PropTypes from "prop-types";

export default function Modal(props) {
    const { className, header, text, closeButton } = props.modalConfig;
    const { onClose, actions } = props;

        return (
            <div className={"modal " + className} onClick={() => {onClose()}}>
                <div className="modal__window" onClick={(event) => event.stopPropagation()}>
                    <div className="modal__header">
                        <div className="modal-window__header-text">{header}</div>
                        {closeButton && <svg onClick={() => {onClose()}} className="modal-window__close-btn close-btn" width="20" height="20" viewBox="0 0 26 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1 1L23.5 24" stroke="white" strokeWidth="2" />
                            <path d="M1 23L24 1" stroke="white" strokeWidth="2" />
                        </svg>}
                    </div>
                    <p className="modal__content">{text}</p>
                    {actions}
                </div>
            </div>
        )
}

Modal.propTypes = {
    modalConfig: PropTypes.exact({
        id: PropTypes.string,
        className: PropTypes.string,
        header: PropTypes.string,
        closeButton: PropTypes.bool,
        text: PropTypes.string
    })
}

// Modal.defaultProps = {
//     modalConfig: {
//         id: "2",
//         className: "modal--delete",
//         header: "Error",
//         closeButton: true,
//         text: "This modal window was called from default props. Submit the error?"
//     }
// }