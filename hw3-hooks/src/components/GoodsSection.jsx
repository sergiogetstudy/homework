import React, { useContext } from "react";
import Card from "./Card";
import "./GoodsSection.scss";
import PropTypes from 'prop-types';
import { ShoppingContext } from "../App";

export default function GoodsSection() {
  const { goodsList } = useContext(ShoppingContext);

  return (
    <section className="goods-section">
      <h2 className="goods-section__heading">Смартфони</h2>
      <ul className="goods-section__list">
        {goodsList?.map(item => (
          <Card 
            key={item.id} 
            data={item} 
          />))}
      </ul>
    </section>
  )
}

GoodsSection.propTypes = {
  goods: PropTypes.array,
  favouriteGoods: PropTypes.array,
  handleAddToCard: PropTypes.func
}

GoodsSection.defaultProps = {
  goodsList: [
    {
      name: "<назва смартфону>",
      price: "<не вказана>",
      imgPath: "./imgs/default-phone.png",
      id: "<не вказано>",
      color: "<не вказано>"
    }
  ]
}