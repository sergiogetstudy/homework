import React from "react";
import "./Button.scss";
import propTypes from "prop-types";

export default function Button(props) {

    const {
        modalid, 
        itemid, 
        backgroundColor, 
        className,  
        onClick, 
        text
    } = props;

    return (
        <button 
            data-modalid={modalid} 
            data-itemid={itemid} 
            className={className} 
            style={{backgroundColor}} 
            onClick={onClick}
        >{text}</button>
    );
}

Button.propTypes = {
    modalId: propTypes.string,
    itemId: propTypes.string,
    backgroundColor: propTypes.string,
    className: propTypes.string,
    onClick: propTypes.func,
    text: propTypes.string
}

Button.defaultProps = {
    modalId: "1",
    itemId: "",
    backgroundColor: "black",
    className: "card__button",
    text: "Add to cart"
}