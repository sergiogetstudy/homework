import React, { useContext } from "react";
import { FaBackspace } from "react-icons/fa";
import { ShoppingContext } from "../App";
import CardInCart from "./CardInCart";
import "./CartSection.scss";

export default function CartSection() {

  const { goodsList, goodsOnCartId } = useContext(ShoppingContext);
  
  const goodsMapAmount = new Map();

  goodsList.forEach((item) => {
    let amount = 0;
    [...goodsOnCartId].forEach(onCartId => {
      if(onCartId === item.id) {
        amount++;
        item.amount = amount;
        goodsMapAmount.set(item, amount);
      };
    });
  });

  return (
    <section className="cart-section">
      <h2>Кошик</h2>
      {goodsMapAmount.size === 0 ? 
        <div>Немає товарів в кошику</div> : 
        <div className="cart-section__cart-grid">
          <span className="cart-grid__cell cell__head cell__center">Зображення</span>
          <span className="cart-grid__cell cell__head">Назва</span>
          <span className="cart-grid__cell cell__head cell__center">Артикул</span>
          <span className="cart-grid__cell cell__head cell__center">Кількість</span>
          <span className="cart-grid__cell cell__head cell__center">Ціна</span>
          <span className="cart-grid__cell cell__head cell__center">Сума</span>
          <span className="cart-grid__cell cell__head cell__center"><FaBackspace /></span>
          {Array.from(goodsMapAmount, function mapFn(element) {
            const [item, amount] = element;
            return <CardInCart item={item} amount={amount} key={item.id} />;
          })}
        </div>
      }
    </section>
  )
}