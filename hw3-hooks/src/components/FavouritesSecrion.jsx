import React, { useContext } from "react";
import { ShoppingContext } from "../App";
import Card from "./Card";
import "./FavouritesSection.scss";

export default function FavouritesSecrion() {
  const { goodsList, favouriteGoods } = useContext(ShoppingContext);
  const favouriteArray = goodsList.filter(item => favouriteGoods.includes(item.id));

  return (
    <section className="favourites-section">
      <h2 className="">Обрані товари</h2>
      {favouriteArray.length === 0 ? 
        <div>Немає обраних товарів</div> : 
        <ul className="goods-section__list">
        {favouriteArray?.map(item => (
          <Card 
            key={item.id} 
            data={item} 
          />))}
      </ul>}
    </section>
  )
}