import { useState } from "react";
import modalWindowDeclarations from "../configs/modalConfigs.json";

export default function useModal(goodsOnCartId, setGoodsOnCartId) {
  const [isModalShown, setIsModalShown] = useState(null);
  const [itemToCart, setItemToCart] = useState(null);

  const handleClose = () => {
    setIsModalShown(null);
    setItemToCart(null);
    console.log("modal hidden");
  }

  const handleAddToCart = () => {
    setGoodsOnCartId([...goodsOnCartId, itemToCart]);
    setItemToCart(null);
    handleClose();
    console.log("added to cart");
  }

  const handleDeleteFromCart = () => {
    setGoodsOnCartId(goodsOnCartId.filter((item) => {
      return item !== itemToCart;
    }));
    setItemToCart(null);
    handleClose();
    console.log("delete from cart");
  }

  const renderModal = (e) => {
    const modalid = e.target.dataset.modalid;
    setIsModalShown(modalWindowDeclarations[modalid - 1]);
    setItemToCart(e.target.dataset.itemid);
    console.log("modal is shown");
  }

  const actions = (
    <div>
        <button 
          className="modal__button" 
          onClick={() => {
            switch (isModalShown.id) {
            case "1":
              handleAddToCart();
              break;
            case "2":
              handleDeleteFromCart();
              break;
          }}}
        >Ok</button>
        <button 
          className="modal__button" 
          onClick={() => {handleClose()}}
        >Cancel</button>
    </div>
  )
  return [actions, isModalShown, renderModal, handleClose];
}