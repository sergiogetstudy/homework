import { useEffect } from "react";

export default function useLocalStorage(map) {
  useEffect(() => {
    for(let [key, value] of map.entries()) {
      if (value?.length === 0) continue;
      localStorage.setItem(key, value);
    }
    console.log("local stored");
  }, [...map.values()]);
}