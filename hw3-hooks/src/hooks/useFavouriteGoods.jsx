export default function useFavouriteGoods(favouriteGoods, setFavouriteGoods) {

  const toggleToFavourite = (id) => {
    const isChecked = favouriteGoods.find(favourite => favourite === id);

    if(isChecked) {
      const newFavouriteList = favouriteGoods.filter(favourite => favourite !== id);
      setFavouriteGoods(newFavouriteList);
      console.log("unchecked");
    } else {
      setFavouriteGoods([...favouriteGoods, id]);
      console.log("checked");
    }
  }
  return [toggleToFavourite];
}