import { useState, useEffect } from "react";

export default function useGoodsList(path) {
  const [goodsList, setGoodsList] = useState([]);
  const [isLoaded, setIsLoaded] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    console.log("fetchig...");
    fetch(path)
      .then(response => response.json())
      .then(data => {
        setGoodsList([...goodsList, ...data]);
        setIsLoaded(true);
        console.log("fetched");
    },
      (error) => {
        setIsLoaded(true);
        setError(error);
      }
    );
  }, [path]);
  return [goodsList, isLoaded, error];
}