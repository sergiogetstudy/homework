import './App.scss';
import React, { createContext, useState } from 'react';
import Header from './components/Header';
import GoodsSection from './components/GoodsSection';
import Modal from "./components/Modal.jsx"

import useGoodsList from './hooks/useGoodsList';
import useFavouriteGoods from './hooks/useFavouriteGoods';
import useLocalStorage from './hooks/useLocalStorage';
import useModal from './hooks/useModal';
import { Route, Routes } from 'react-router';
import CartSection from './components/CartSection';
import FavouritesSecrion from './components/FavouritesSecrion';
import { useEffect } from 'react/cjs/react.development';

export const ShoppingContext = createContext();

export default function App() {
  
  const [goodsOnCartId, setGoodsOnCartId] = 
    useState(localStorage.getItem("goodsOnCartId") ? 
      localStorage.getItem("goodsOnCartId").split(",") : 
      []);
  const [favouriteGoods, setFavouriteGoods] = 
    useState(localStorage.getItem("favouriteGoods") ? 
      localStorage.getItem("favouriteGoods").split(",") : 
      []);

  const [goodsList, isLoaded, error] = useGoodsList("./configs/smartphones.json");
  const [toggleToFavourite] = useFavouriteGoods(favouriteGoods, setFavouriteGoods);
  const [actions, isModalShown, renderModal, handleClose] = useModal(goodsOnCartId, setGoodsOnCartId);

  useLocalStorage(new Map([
    ['goodsOnCartId', goodsOnCartId],
    ['favouriteGoods', favouriteGoods]
  ]));

  useEffect(() => {
    localStorage.setItem("goodsOnCartId", goodsOnCartId)
  }, [goodsOnCartId]);

  useEffect(() => {
    localStorage.setItem("favouriteGoods", favouriteGoods)
  }, [favouriteGoods]);

  if(error) {
    return <div>Помилка: {error.massage}</div>

  } else if(!isLoaded) {
    return <div>Завантаження...</div>

  } else {
    return (
      <ShoppingContext.Provider value={{ goodsList, goodsOnCartId, setGoodsOnCartId, favouriteGoods, toggleToFavourite, isModalShown, renderModal }}>
        <>
        <Header />
        <Routes>
          <Route
            path="/"
            element={<GoodsSection />}
          />
          <Route 
            path="/cart"
            element={<CartSection />}
          />
          <Route 
            path="/favourites"
            element={<FavouritesSecrion />}
          />
        </Routes>
        {isModalShown && 
          <Modal 
            modalConfig={isModalShown} 
            onClose={handleClose} 
            actions={actions} 
          />}
        </>
      </ShoppingContext.Provider>
    );
  }
}
