const centeredContent = document.querySelector('.tabs');

const showDescription = function (event){

    const activeTabs = document.querySelectorAll('.active');
    for (let active of activeTabs){
        active.classList.remove("active");
    }

    const currentItem = event.target.closest('.tabs-title');
    currentItem.classList.add("active");

    const id = currentItem.innerHTML;

    const tabContentItems = document.querySelectorAll('.tabs-content-item');
    for (let contentItem of tabContentItems){
        if(contentItem.dataset.descriptionItem === id){
            contentItem.classList.remove("not-displayed");
        } else {
            contentItem.classList.add("not-displayed");
        }
    }

    const tabTitles = document.querySelectorAll(".tabs-title");
    for (let title of tabTitles){
        title.classList.remove("active");
    }
    currentItem.classList.add("active");
}

centeredContent.addEventListener('click', showDescription);
