/*Обробник подій - це блок коду, який виконується при спрацюванні події. Події викинкають в результаті дій користувача. Кожна подія являє собою об'єкт, який в свою чергу має додаткові поля і функції, які дозволяють визначити що сталося. Для захоплення події є декілька методів оголошення обробника подій, найбільш пріорітетнішим з яких є реєстрація обробника (додавання елементу "слухача") методом .addEventListener(). Цей метод має такі вхідні параметри, як тип події та функцію, яка має бути виконана в при спрацюванні події.
*/

const priceInput = document.querySelector("#price-value");
const inputPriceError = document.querySelector("#error");
const priceInputLabel = document.querySelector("#price-label");
const priceInputContainer = document.querySelector("#price-value-container");
const error = document.querySelector("#error");

function createAndAppendElement(tagName, textContent, parentElement, classNames = [], attributes = {}){
    let outputPriceSpan = document.createElement(tagName);
    parentElement.append(outputPriceSpan);
    outputPriceSpan.innerHTML = textContent;

    for(let className of classNames){
        outputPriceSpan.classList.add(className);
    }
    for(key in attributes){
        outputPriceSpan.setAttribute(key, attributes[key]);
    }
}

function isValidNumber(checkValue){
    if (Number.isNaN(+checkValue) || checkValue < 0){
        return false;
    } else {
        return true;
    }
}

priceInput.addEventListener("focus", function () {
    priceInput.value = "";
    priceInput.classList.add("focus-input")} );

priceInput.addEventListener("blur", function (){
    priceInput.classList.remove("focus-input");
    if(!isValidNumber(priceInput.value)){
        for (className of priceInput.classList){
            if (className = "invalid-input"){
                return;
            }
        }
        priceInput.classList.add("invalid-input");
        createAndAppendElement("span", `Please enter correct price`, error, ["error"]);
    } else{
        priceInput.classList.remove("invalid-input");
        inputPriceError.innerHTML = "";
        
        createAndAppendElement("span", `Текущая цена: ${priceInput.value}`, priceInputContainer, ["capture-value"], {name: "hello"});
        createAndAppendElement("span", `X`, priceInputContainer.lastChild, ["closer-btn"])
    }
});

priceInputContainer.addEventListener("click", function (event) {
    if (event.target.className != 'closer-btn'){
        return;
    }

    let addedSpan = event.target.closest('.closer-btn');
    addedSpan.parentElement.remove();
});
