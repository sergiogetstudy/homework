const buttons = document.querySelectorAll(".btn");



const eventHandler = function(event) {
    buttons.forEach(function(button) {
        const atributeKey = button.getAttribute("data-key");
        if(atributeKey === event.code) {
            button.classList.add("btn-on-key");
        } else {
            button.classList.remove("btn-on-key");
        }
    })
}

document.addEventListener("keydown", eventHandler);

