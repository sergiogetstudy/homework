package com.sergiogetstudy.homework6;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WomanTest {
    Man simpleFather;
    Woman simpleMother;
    Family simpleFamily;
    Woman karen;
    Man hank;
    Family moodies;


    @BeforeEach
    void setUp() {
        simpleFather = new Man("", "", (short) 0);
        simpleMother = new Woman("", "", (short) 0);
        simpleFamily = new Family(simpleMother, simpleFather);
        karen = new Woman("Karen", "Van der Beek",
                (short) 1981, (byte) 127,
                new String[][] {}, simpleFamily, new Dog(""), simpleMother, simpleFather);
        hank = new Man("Hank", "Moody", (short) 1980, (byte) 27,
                new String[][] {}, simpleFamily, new Dog(""), simpleMother, simpleFather);
        moodies = new Family(karen, hank);
    }

    @AfterEach
    void tearDown() {
        simpleFather = null;
        simpleMother = null;
        simpleFamily = null;
        karen = null;
        hank = null;
        moodies = null;
    }


    @Test
    void bornChild() {
        assertNotNull(simpleMother.bornChild());
        assertNotNull(karen.bornChild());
    }
}