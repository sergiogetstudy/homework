package com.sergiogetstudy.homework6;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class HumanTest {
    Human simpleHuman;
    Human humanAndParents;
    Human humanAllDataInit;
    Human humanAllDataVarName;
    Human humanAllDataVarSurname;
    Human humanAllDataVarYear;
    Human humanAllDataVarIq;
    Human humanAllDataVarSchedule;
    Human humanAllDataVarFamily;
    Human humanAllDataVarPet;
    Human humanAllDataVarMother;
    Human humanAllDataVarFather;
    @BeforeAll
    static void beforeAll() { System.out.println("Testing Human class..."); }

    @BeforeEach
    void setUp() {
        simpleHuman = new Human("", "", (short) 0);
        humanAndParents = new Human("", "", (short) 0, simpleHuman, simpleHuman);
        humanAllDataInit = new Human("", "", (short) 0, (byte) 0, new String[][]{},
                new Family(simpleHuman, simpleHuman), new Dog(), simpleHuman, simpleHuman);
        humanAllDataVarName = new Human("1", "", (short) 0, (byte) 0, new String[][]{},
                new Family(simpleHuman, simpleHuman), new Dog(), simpleHuman, simpleHuman);
        humanAllDataVarSurname = new Human("", "1", (short) 0, (byte) 0, new String[][]{},
                new Family(simpleHuman, simpleHuman), new Dog(), simpleHuman, simpleHuman);
        humanAllDataVarYear = new Human("", "", (short) 1, (byte) 0, new String[][]{},
                new Family(simpleHuman, simpleHuman), new Dog(), simpleHuman, simpleHuman);
        humanAllDataVarIq = new Human("", "", (short) 0, (byte) 1, new String[][]{},
                new Family(simpleHuman, simpleHuman), new Dog(), simpleHuman, simpleHuman);
        humanAllDataVarSchedule = new Human("", "", (short) 0, (byte) 0, new String[][]{{"1"}, {"1"}},
                new Family(simpleHuman, simpleHuman), new Dog(), simpleHuman, simpleHuman);
        humanAllDataVarFamily = new Human("", "", (short) 0, (byte) 0, new String[][]{},
                new Family(humanAllDataVarSurname, humanAllDataVarSurname), new Dog(), simpleHuman, simpleHuman);
        humanAllDataVarPet = new Human("", "", (short) 0, (byte) 0, new String[][]{},
                new Family(simpleHuman, simpleHuman), new Dog(""), simpleHuman, simpleHuman);
        humanAllDataVarMother = new Human("", "", (short) 0, (byte) 0, new String[][]{},
                new Family(simpleHuman, simpleHuman), new Dog(), humanAllDataVarSurname, simpleHuman);
        humanAllDataVarFather = new Human("", "", (short) 0, (byte) 0, new String[][]{},
                new Family(simpleHuman, simpleHuman), new Dog(), simpleHuman, humanAllDataVarSurname);
    }

    @Test
    void testToString() {
        assertEquals(simpleHuman.toString(),
                "Human{name='', surname='', year=0, iq=0, father=null, mother=null}");

        assertEquals(humanAndParents.toString(),
                "Human{name='', surname='', year=0, iq=0, father= , mother= }");

        assertEquals(humanAllDataInit.toString(),
                "Human{name='', surname='', year=0, iq=0, father= , mother= }");
    }

    @Test
    void testEquals() {
        assertNotEquals(humanAllDataInit, humanAllDataVarName);
        assertEquals(humanAllDataInit, humanAllDataVarSurname);
        assertNotEquals(humanAllDataInit, humanAllDataVarYear);
        assertEquals(humanAllDataInit, humanAllDataVarIq);
        assertEquals(humanAllDataInit, humanAllDataVarSchedule);
        assertEquals(humanAllDataInit, humanAllDataVarFamily);
        assertEquals(humanAllDataInit, humanAllDataVarPet);
        assertEquals(humanAllDataInit, humanAllDataVarMother);
        assertEquals(humanAllDataInit, humanAllDataVarFather);
    }

    @Test
    void testHashCode() {
        assertNotEquals(humanAllDataInit.hashCode(), humanAllDataVarName.hashCode());
        assertEquals(humanAllDataInit.hashCode(), humanAllDataVarSurname.hashCode());
        assertNotEquals(humanAllDataInit.hashCode(), humanAllDataVarYear.hashCode());
        assertEquals(humanAllDataInit.hashCode(), humanAllDataVarIq.hashCode());
        assertEquals(humanAllDataInit.hashCode(), humanAllDataVarSchedule.hashCode());
        assertEquals(humanAllDataInit.hashCode(), humanAllDataVarFamily.hashCode());
        assertEquals(humanAllDataInit.hashCode(), humanAllDataVarPet.hashCode());
        assertEquals(humanAllDataInit.hashCode(), humanAllDataVarMother.hashCode());
        assertEquals(humanAllDataInit.hashCode(), humanAllDataVarFather.hashCode());
    }

    @AfterEach
    void tearDown() {
        simpleHuman = null;
        humanAndParents = null;
        humanAllDataInit = null;
        humanAllDataVarName = null;
        humanAllDataVarSurname = null;
        humanAllDataVarYear = null;
        humanAllDataVarIq = null;
        humanAllDataVarSchedule = null;
        humanAllDataVarFamily = null;
        humanAllDataVarPet = null;
        humanAllDataVarMother = null;
        humanAllDataVarFather = null;
    }

    @AfterAll
    static void afterAll() {
        System.out.println("Finish testing Human class!");
    }
}