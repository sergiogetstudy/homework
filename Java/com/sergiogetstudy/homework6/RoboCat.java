package com.sergiogetstudy.homework6;

public class RoboCat extends Pet {
    Species species = Species.ROBO_CAT;
    public RoboCat() {}

    public RoboCat(String nickname) {
        super(nickname);
    }

    public RoboCat(String nickname, int age, byte trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public Species getSpecies() { return this.species; }
    @Override
    public void respond() {
        System.out.printf("Hello, host. I'm - %s. Can I help you?%n", getNickname());
    }
}
