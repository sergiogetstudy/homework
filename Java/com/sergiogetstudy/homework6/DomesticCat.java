package com.sergiogetstudy.homework6;

public class DomesticCat extends Pet implements Foulable {
    Species species = Species.DOMESTIC_CAT;

    public DomesticCat() {}
    public DomesticCat(String nickname) {
        super(nickname);
    }
    public DomesticCat(String nickname, int age, byte trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public Species getSpecies() { return this.species; }
    @Override
    public void respond() {
        System.out.printf("Hello slave. I'm - %s. Fetch some food! Meow%n", getNickname());
    }
    @Override
    public void foul() {
        System.out.println("Slave, clean it up");
    }
}
