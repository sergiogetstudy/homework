package com.sergiogetstudy.homework8.domain.interfaces;

import com.sergiogetstudy.homework8.domain.human.Human;

public interface HumanCreator {
    Human bornChild(String childMansName, String childWomansName);
}
