package com.sergiogetstudy.homework8.domain.pet.fish;

import com.sergiogetstudy.homework8.domain.pet.Pet;
import com.sergiogetstudy.homework8.domain.enums.Species;

import java.util.Set;

public class Fish extends Pet {
    final Species species = Species.FISH;

    public Fish() {}
    public Fish(String nickname) {
        super(nickname);
    }
    public Fish(String nickname, int age, byte trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public Species getSpecies() { return this.species; }
    @Override
    public void respond() {
        System.out.printf("Bul bul");
    }
}
