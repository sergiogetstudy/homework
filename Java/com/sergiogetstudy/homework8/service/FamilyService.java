package com.sergiogetstudy.homework8.service;

import com.sergiogetstudy.homework8.domain.pet.Pet;
import com.sergiogetstudy.homework8.domain.human.Human;
import com.sergiogetstudy.homework8.dao.FamilyDao;
import com.sergiogetstudy.homework8.domain.family.Family;
import com.sergiogetstudy.homework8.domain.human.woman.Woman;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FamilyService {
    private FamilyDao familyDao;

    public void setFamilyDao(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }
    public String displayAllFamilies() {
        List<Family> families = familyDao.getAllFamilies();
        StringBuilder result = new StringBuilder();
        for(int i = 0; i < families.size(); i++) {
            result.append(i).append(" - ").append(families.get(i)).append("\n");
        }
        System.out.println(result);

        return result.toString();
    }
    public List<Family> getFamiliesBiggerThan(int count) {
        List<Family> families = familyDao.getAllFamilies();
        List<Family> filteredList = new ArrayList<>();

        for(int i = 0; i < families.size(); i++) {
            Family family = families.get(i);

            if(family.countFamily() > count) {
                filteredList.add(family);
                System.out.println(i + " - " + family);
            }
        }

        return filteredList;
    }
    public List<Family>  getFamiliesLessThan(int count) {
        List<Family> families = familyDao.getAllFamilies();
        List<Family> filteredList = new ArrayList<>();

        for(int i = 0; i < families.size(); i++) {
            Family family = families.get(i);

            if(family.countFamily() < count) {
                filteredList.add(family);
                System.out.println(i + " - " + family);
            }
        }
        return filteredList;
    }

    public int countFamiliesWithMemberNumber(int memberNumber) {
        int count = 0;
        for (Family family: familyDao.getAllFamilies()) {
            if(family.countFamily() == memberNumber) count++;
        }
        return count;
    }
    public Family createNewFamily(Human woman, Human man) {
        if(woman == null || man == null) return null;
        Family family = new Family(woman, man);
        familyDao.saveFamily(family);
        return family;
    }
    public boolean deleteFamilyByIndex(int index) {
        return familyDao.deleteFamily(index);
    }
    public Family bornChild(Family family, String childMansName, String childWomansName) {
        if(family == null) return null;
        Human child = ((Woman) family.getMother()).bornChild(childMansName, childWomansName);
        family.addChild(child);
        if (familyDao.getAllFamilies().contains(family)) familyDao.saveFamily(family);

        return family;
    }
    public Family adoptChild(Family family, Human child) {
        if(family == null) return null;

        family.addChild(child);
        if (familyDao.getAllFamilies().contains(family)) familyDao.saveFamily(family);

        return family;
    }
    public boolean deleteAllChildrenOlderThen(int childAge) {
        List<Family> families = familyDao.getAllFamilies();
        boolean isChanged = false;
        for (Family family : families) {
            for (Human child : family.getChildren()) {
                if(child.getYear() > childAge) {
                    family.deleteChild(child);
                    familyDao.saveFamily(family);
                    isChanged = true;
                };
            }
        }
        return isChanged;
    }
    public int count() {
        return familyDao.getAllFamilies().size();
    }
    public Family getFamilyById(int index) {
        return familyDao.getFamilyByIndex(index);
    }
    public Set<Pet> getPets(int index) {
        if(familyDao.getFamilyByIndex(index) == null) return null;
        return familyDao.getFamilyByIndex(index).getPets();
    }
    public boolean addPet(int index, Pet pet) {
        if(pet == null) return false;
        Family family = familyDao.getFamilyByIndex(index);
        Set<Pet> pets = family.getPets();
        if(pets == null) {
            family.setPets(new HashSet<>());
            pets = family.getPets();
        };
        boolean isChanged = pets.add(pet);
        family.setPets(pets);
        familyDao.saveFamily(family);

        return isChanged;
    }
}
