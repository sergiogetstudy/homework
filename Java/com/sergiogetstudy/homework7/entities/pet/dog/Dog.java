package com.sergiogetstudy.homework7.entities.pet.dog;

import com.sergiogetstudy.homework7.entities.pet.Pet;
import com.sergiogetstudy.homework7.interfaces.Foulable;
import com.sergiogetstudy.homework7.enums.Species;

import java.util.Set;

public class Dog extends Pet implements Foulable {
    final Species species = Species.DOG;;
    public Dog() {}

    public Dog(String nickname) {
        super(nickname);
    }

    public Dog(String nickname, int age, byte trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public Species getSpecies() { return this.species; }

    @Override
    public void respond() {
        System.out.printf("Hello, host. I'm - %s. I missed!%n", getNickname());
    }

    @Override
    public void foul() {
        System.out.println("It is necessary to replace traces well...");
    }
}
