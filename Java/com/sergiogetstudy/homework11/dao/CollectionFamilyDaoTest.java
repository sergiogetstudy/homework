package com.sergiogetstudy.homework11.dao;

import com.sergiogetstudy.homework11.domain.family.Family;
import com.sergiogetstudy.homework11.domain.human.Human;
import com.sergiogetstudy.homework11.domain.human.man.Man;
import com.sergiogetstudy.homework11.domain.human.woman.Woman;
import com.sergiogetstudy.homework11.domain.pet.Pet;
import com.sergiogetstudy.homework11.domain.pet.dog.Dog;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class CollectionFamilyDaoTest {
    Human charlie, marcy;
    Human andrew, jane;
    Human al, margaret;
    Human karen, hank;
    Pet catStevens;
    Set<Pet> moodiesPets;
    Family runkles;
    Family vanDerBeek;
    Family moodiesOlder;
    Family moodies;

    List<Family> families;
    CollectionFamilyDao collectionFamilyDaoTest;


    @BeforeEach
    void setUp() {
        charlie = new Man("Charlie", "Runkle", (short) 1983);
        marcy = new Woman("Marcy", "Runkle", (short) 1984);
        runkles = new Family(marcy, charlie);

        andrew = new Man("Andrew", "Van der Beek", (short) 1953);
        jane = new Woman("Jane", "Van der Beek", (short) 1957);
        vanDerBeek= new Family(jane, andrew);

        al = new Man("Al", "Moody", (short) 1950);
        margaret = new Woman("Margaret", "Moody", (short) 1951);
        moodiesOlder = new Family(margaret, al);

        catStevens = new Dog("Cat Stevens",
                3, (byte) 30, new HashSet<>(Arrays.asList("watch films", "eat shoes")));
        moodiesPets = new HashSet<>();
        moodiesPets.add(catStevens);
        karen = new Woman("Karen", "Van der Beek", (short) 1981);
        hank = new Man("Hank", "Moody", (short) 1980, (byte) 27,
                new HashMap<>(), moodiesOlder, moodiesPets);
        moodies = new Family(karen, hank);

        families = new ArrayList<>();
        families.add(runkles);
        families.add(vanDerBeek);
        families.add(moodiesOlder);
        families.add(moodies);

        collectionFamilyDaoTest = new CollectionFamilyDao();
    }

    @Test
    void testGetAllFamilies() {
        assertArrayEquals(collectionFamilyDaoTest.getAllFamilies().toArray(), new ArrayList().toArray());
        collectionFamilyDaoTest.saveFamily(runkles);
        collectionFamilyDaoTest.saveFamily(vanDerBeek);
        collectionFamilyDaoTest.saveFamily(moodiesOlder);
        collectionFamilyDaoTest.saveFamily(moodies);
        assertArrayEquals(collectionFamilyDaoTest.getAllFamilies().toArray(), families.toArray());
    }

    @Test
    void testGetFamilyByIndex() {
        assertNull(collectionFamilyDaoTest.getFamilyByIndex(-1));
        assertNull(collectionFamilyDaoTest.getFamilyByIndex(0));
        collectionFamilyDaoTest.saveFamily(runkles);
        assertNull(collectionFamilyDaoTest.getFamilyByIndex(-1));
        assertNull(collectionFamilyDaoTest.getFamilyByIndex(1));
        assertEquals(collectionFamilyDaoTest.getFamilyByIndex(0), runkles);
    }

    @Test
    void testDeleteFamily() {
        assertFalse(collectionFamilyDaoTest.deleteFamily(-1));
        assertFalse(collectionFamilyDaoTest.deleteFamily(0));

        assertFalse(collectionFamilyDaoTest.deleteFamily(runkles));

        collectionFamilyDaoTest.saveFamily(runkles);
        collectionFamilyDaoTest.saveFamily(vanDerBeek);
        collectionFamilyDaoTest.saveFamily(moodiesOlder);
        assertFalse(collectionFamilyDaoTest.deleteFamily(3));
        assertTrue(collectionFamilyDaoTest.deleteFamily(2));
        assertEquals(collectionFamilyDaoTest.getAllFamilies().size(), 2);

        assertTrue(collectionFamilyDaoTest.deleteFamily(runkles));
        assertEquals(collectionFamilyDaoTest.getAllFamilies().size(), 1);
    }

    @Test
    void testSaveFamily() {
        collectionFamilyDaoTest.saveFamily(runkles);
        collectionFamilyDaoTest.saveFamily(vanDerBeek);
        collectionFamilyDaoTest.saveFamily(moodiesOlder);
        assertEquals(collectionFamilyDaoTest.getAllFamilies().size(), 3);
        assertEquals(collectionFamilyDaoTest.getFamilyByIndex(2), moodiesOlder);
        collectionFamilyDaoTest.saveFamily(moodiesOlder);
        assertEquals(collectionFamilyDaoTest.getAllFamilies().size(), 3);
    }

    @AfterEach
    void tearDown() {
        charlie = null; marcy = null;
        andrew = null; jane = null;
        al = null; margaret = null;
        karen = null; hank = null;
        catStevens = null;
        moodiesPets = null;
        runkles = null; vanDerBeek = null; moodiesOlder = null; moodies = null;
        families = null;
    }
}