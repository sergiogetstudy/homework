package com.sergiogetstudy.homework11.domain.family;

public class FamilyOverflowException extends RuntimeException {
    public FamilyOverflowException(String message) {
        super(message);
    }
}
