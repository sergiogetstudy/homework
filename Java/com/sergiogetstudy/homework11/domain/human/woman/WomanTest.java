package com.sergiogetstudy.homework11.domain.human.woman;

import com.sergiogetstudy.homework11.domain.family.Family;
import com.sergiogetstudy.homework11.domain.human.man.Man;
import com.sergiogetstudy.homework11.domain.pet.Pet;
import com.sergiogetstudy.homework11.domain.pet.dog.Dog;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class WomanTest {
    Man simpleFather;
    Woman simpleMother;
    Family simpleFamily;
    Woman karen;
    Man hank;
    Family moodies;
    Set<Pet> pets;


    @BeforeEach
    void setUp() {
        simpleFather = new Man("", "", (short) 0);
        simpleMother = new Woman("", "", (short) 0);
        simpleFamily = new Family(simpleMother, simpleFather);
        pets = new HashSet<>();
        pets.add(new Dog(""));
        karen = new Woman("Karen", "Van der Beek",
                (short) 1981, (byte) 127,
                new HashMap<>(), simpleFamily, pets);
        hank = new Man("Hank", "Moody", (short) 1980, (byte) 27,
                new HashMap<>(), simpleFamily, pets);
        moodies = new Family(karen, hank);
    }

    @AfterEach
    void tearDown() {
        simpleFather = null;
        simpleMother = null;
        simpleFamily = null;
        karen = null;
        hank = null;
        moodies = null;
    }


    @Test
    void bornChild() {
        assertNotNull(simpleMother.bornChild("", ""));
        assertNotNull(karen.bornChild("", ""));
    }
}