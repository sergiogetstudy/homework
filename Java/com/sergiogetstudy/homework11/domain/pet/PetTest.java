package com.sergiogetstudy.homework11.domain.pet;

import com.sergiogetstudy.homework11.domain.pet.dog.Dog;
import com.sergiogetstudy.homework11.domain.pet.domestic_cat.DomesticCat;
import com.sergiogetstudy.homework11.domain.pet.robo_cat.RoboCat;
import org.junit.jupiter.api.*;

import java.util.Arrays;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class PetTest {
    Pet simplePet;
    Pet pet;
    @BeforeAll
    static void beforeAll() { System.out.println("Testing Pet class..."); }
    @BeforeEach
    void setUp() {
        simplePet = new DomesticCat();
        pet = new Dog("1", 0, (byte) 50, new HashSet<>() {
        });
    }

    @Test
    void testToString() {
        Assertions.assertEquals(simplePet.toString(),
                "Species{species='cat', can fly=false, number of legs=4, has fur=true}" +
                        "{nickname='null', age=0, trickLevel=not very cunning, habits=null}");

        Pet speciesNickname = new DomesticCat(null);
        Assertions.assertEquals(speciesNickname.toString(),
                "Species{species='cat', can fly=false, number of legs=4, has fur=true}" +
                        "{nickname='null', age=0, trickLevel=not very cunning, habits=null}");

        Pet emptyNameMin = new Dog(null, -1, (byte) -1, new HashSet<>());
        Assertions.assertEquals(emptyNameMin.toString(),
                "Species{species='dog', can fly=false, number of legs=4, has fur=true}" +
                        "{nickname='null', age=0, trickLevel=not very cunning, habits=[]}");

        Pet emptyNameMax = new RoboCat("", -1, (byte) 101, new HashSet<>()) {

        };
        Assertions.assertEquals(emptyNameMax.toString(),
                "Species{species='robo cat', can fly=false, number of legs=4, has fur=false}" +
                        "{nickname='doesn't have nickname yet', age=0, trickLevel=very cunning, habits=[]}");
    }

    @Test
    void testEquals() {
        assertNotEquals(pet, new DomesticCat("1", 0, (byte) 50, new HashSet<>()));
        assertNotEquals(pet, new Dog("", 0, (byte) 50, new HashSet<>()));
        assertNotEquals(pet, new Dog("1", 1, (byte) 50, new HashSet<>()));
        assertEquals(pet, new Dog("1", 0, (byte) 70, new HashSet<>()));
        assertEquals(pet, new Dog("1", 0, (byte) 50, new HashSet<>(Arrays.asList("1", "2"))));
    }

    @Test
    void testHashCode() {
        assertEquals(pet.hashCode(), new DomesticCat("1", 0, (byte) 50, new HashSet<>()).hashCode());
        assertNotEquals(pet.hashCode(), new Dog("", 0, (byte) 50, new HashSet<>()).hashCode());
        assertEquals(pet.hashCode(), new Dog("1", 1, (byte) 50, new HashSet<>()).hashCode());
        assertEquals(pet.hashCode(), new Dog("1", 0, (byte) 70, new HashSet<>()).hashCode());
        assertEquals(pet.hashCode(), new Dog("1", 0, (byte) 50, new HashSet<>(Arrays.asList("1", "2"))).hashCode());
    }

    @AfterEach
    void tearDown() { simplePet = null; pet = null; }
    @AfterAll
    static void afterAll() { System.out.println("Finish testing Pet class!"); }
}