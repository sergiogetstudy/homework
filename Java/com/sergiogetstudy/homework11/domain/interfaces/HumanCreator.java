package com.sergiogetstudy.homework11.domain.interfaces;

import com.sergiogetstudy.homework11.domain.human.Human;

public interface HumanCreator {
    Human bornChild(String childMansName, String childWomansName);
}
