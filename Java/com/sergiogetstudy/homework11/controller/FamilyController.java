package com.sergiogetstudy.homework11.controller;

import com.sergiogetstudy.homework11.domain.enums.DayOfWeek;
import com.sergiogetstudy.homework11.domain.family.Family;
import com.sergiogetstudy.homework11.domain.family.FamilyOverflowException;
import com.sergiogetstudy.homework11.domain.human.Human;
import com.sergiogetstudy.homework11.domain.human.man.Man;
import com.sergiogetstudy.homework11.domain.human.woman.Woman;
import com.sergiogetstudy.homework11.domain.pet.Pet;
import com.sergiogetstudy.homework11.domain.pet.dog.Dog;
import com.sergiogetstudy.homework11.domain.pet.domestic_cat.DomesticCat;
import com.sergiogetstudy.homework11.service.FamilyService;

import java.time.format.DateTimeParseException;
import java.util.*;

public class FamilyController {
    FamilyService familyService;
    private Scanner scanner = new Scanner(System.in);

    public void setFamilyService(FamilyService familyService) {
        this.familyService = familyService;
    }

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }
    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }
    public List<Family> getFamiliesBiggerThan(int count) {
        return familyService.getFamiliesBiggerThan(count);
    }
    public List<Family>  getFamiliesLessThan(int count) {
        return familyService.getFamiliesLessThan(count);
    }
    public int countFamiliesWithMemberNumber(int memberNumber) {
        return familyService.countFamiliesWithMemberNumber(memberNumber);
    }
    public Family createNewFamily(Human woman, Human man) {
        return familyService.createNewFamily(woman, man);
    }
    public boolean deleteFamilyByIndex(int index) {
        return familyService.deleteFamilyByIndex(index);
    }
    public Family bornChild(Family family, String childMansName, String childWomansName) {
        return familyService.bornChild(family, childMansName, childWomansName);
    }
    public Family adoptChild(Family family, Human child) {
        return familyService.adoptChild(family, child);
    }
    public boolean deleteAllChildrenOlderThen(int childAge) {
        return familyService.deleteAllChildrenOlderThan(childAge);
    }
    public int count() {
        return familyService.count();
    }
    public Family getFamilyById(int index) {
        return familyService.getFamilyById(index);
    }
    public Set<Pet> getPets(int index) {
        return familyService.getPets(index);
    }
    public boolean addPet(int index, Pet pet) {
        return familyService.addPet(index, pet);
    }

    public void setupTestingData() {
        Human charlie = new Man("Charlie", "Runkle", Human.getStamp("01/01/1983", "d/MM/yyyy"));
        Human marcy = new Woman("Marcy", "Runkle", Human.getStamp("1/01/1984", "d/MM/yyyy"));

        createNewFamily(marcy, charlie);
        getFamilyById(0);
        addPet(0, new DomesticCat());

        Human andrew = new Man("Andrew", "Van der Beek", Human.getStamp("01/01/1953", "d/MM/yyyy"));
        Human jane = new Woman("Jane", "Van der Beek", Human.getStamp("01/01/1983", "d/MM/yyyy"));
        new Family(jane, andrew);
        createNewFamily(jane, andrew);


        Human al = new Man("Al", "Moody", Human.getStamp("01/01/1950", "d/MM/yyyy"));
        Human margaret = new Woman("Margaret", "Moody", Human.getStamp("01/01/1951", "d/MM/yyyy"));
        Family moodiesOlder = createNewFamily(margaret, al);


        Dog catStevens = new Dog("Cat Stevens",
                3, (byte) 30, new HashSet<>(Arrays.asList("watch films", "eat shoes")));
        Set<Pet> moodiesPets = new HashSet<>();
        moodiesPets.add(catStevens);
        Human karen = new Woman("Karen", "Van der Beek", Human.getStamp("01/01/1983", "d/MM/yyyy"));
        Human hank = new Man("Hank", "Moody", Human.getStamp("01/01/1983", "d/MM/yyyy"), (byte) 27,
                createSchedule(), moodiesOlder, moodiesPets);

        createNewFamily(karen, hank);
        addPet(2, catStevens);

        bornChild(getFamilyById(2), "Levon", "Becca");

        Human levon = new Man("Levon", "Moody", "1/12/1999", (byte) 100);

        adoptChild(getFamilyById(2), levon);
    }

    public static Map<String, String> createSchedule() {
        Map<String, String> schedule = new HashMap<>();

        for (DayOfWeek day: DayOfWeek.values()) {
            schedule.put(day.toString(), "");
        }

        return schedule;
    }

    public void printMenu() {
        while (true) {
            System.out.println("1. \uD83C\uDFAC Feel by testing data");
            System.out.println("2. \uD83D\uDCC3 Show full family list");
            System.out.println("3. \uD83D\uDC46 Show family list with persons more, then...");
            System.out.println("4. \uD83D\uDC47 Show family list with persons less, then...");
            System.out.println("5. \uD83D\uDDA9 Count families with persons equal...");
            System.out.println("6. \u2795 Create new family");
            System.out.println("7. \u2796 Delete family by index");
            System.out.println("8. \u270D Edit family by index");
            System.out.println("9. \uD83D\uDEAA Exit");
            System.out.println();

            int choice = intReceiver("Enter the number: ", "Input again: ");
            scanner.nextLine();

            switch (choice) {
                case 1:
                    setupTestingData();
                    System.out.println("Done\uD83D\uDC4D\n");
                    break;

                case 2:
                    displayAllFamilies();
                    break;

                case 3:
                    getFamiliesBiggerThan(intReceiver("Show family list with persons more, then: ", "Input again: "));
                    break;

                case 4:
                    getFamiliesLessThan(intReceiver("Show family list with persons less: ", "Input again: "));
                    break;

                case 5:
                    int count = intReceiver("Count families with persons equal: ", "Input again: ");
                    if(count == 1) System.out.println(count + " family");
                    else System.out.println(count + " families");
                    break;

                case 6:
                    String motherName = stringReceiver("Enter mother`s name: ");
                    String motherSurname = stringReceiver("Enter mother`s surname: ");
                    int motherYearBirthDate = intReceiver("Enter mother`s year of birth date: ", "Input again: ");
                    int motherMonthBirthDate = intReceiver("Enter mother`s month of birth date: ", "Input again: ");
                    int motherDayBirthDate = intReceiver("Enter mother`s day of birth date: ", "Input again: ");
                    byte motherIq = (byte) intReceiver("Enter mother`s IQ: ", "Input again: ");

                    Human mother = getHuman(motherName, motherSurname, motherYearBirthDate, motherMonthBirthDate, motherDayBirthDate, motherIq, true);

                    scanner.nextLine();

                    String fatherName = stringReceiver("Enter father`s name: ");
                    String fatherSurname = stringReceiver("Enter father`s surname: ");
                    int fatherYearBirthDate = intReceiver("Enter father`s year of birth date: ", "Input again: ");
                    int fatherMonthBirthDate = intReceiver("Enter father`s month of birth date: ", "Input again: ");
                    int fatherDayBirthDate = intReceiver("Enter father`s day of birth date: ", "Input again: ");
                    byte fatherIq = (byte) intReceiver("Enter father`s IQ: ", "Input again: ");

                    Human father = getHuman(fatherName, fatherSurname, fatherYearBirthDate, fatherMonthBirthDate, fatherDayBirthDate, fatherIq, false);

                    createNewFamily(mother, father);

                    System.out.println("Done\uD83D\uDC4D\n");
                    break;

                case 7:
                    boolean isSuccess = deleteFamilyByIndex(intReceiver("Enter the index: ", "Input again: "));
                    if (!isSuccess) System.out.println("Fail, family doesn`t exist \uD83D\uDE10");
                    else System.out.println("Done\uD83D\uDC4D\n");
                    break;

                case 8:
                    System.out.println("1. \uD83D\uDC76 Born child");
                    System.out.println("2. \uD83D\uDC6A Adopt child");
                    System.out.println("3. \uD83D\uDD19 Return to main menu");

                    int choiceCase8 = intReceiver("Enter the number: ", "Input again");
                    scanner.nextLine();
                    switch (choiceCase8) {
                        case 1 -> {
                            int familyIndex = intReceiver("Enter family index: ", "Input again: ");
                            String boyName = stringReceiver("Enter boy name: ");
                            scanner.nextLine();
                            String girlName = stringReceiver("Enter girl name: ");
                            try {
                                bornChild(getFamilyById(familyIndex), boyName, girlName);
                                System.out.println("Done\uD83D\uDC4D\n");
                            } catch (FamilyOverflowException familyOverflowException) {
                                System.out.println(familyOverflowException.getMessage() + '\n');
                            }
                        }
                        case 2 -> {
                            int familyInd = intReceiver("Enter family index: ", "Input again: ");
                            String childSex = "";
                            while (!Objects.equals(childSex, "male") && !Objects.equals(childSex, "female")) {
                                scanner.nextLine();
                                childSex = stringReceiver("Enter child`s sex (\"male\" or \"female\"): ");
                            }
                            String childName = stringReceiver("Enter child`s name: ");
                            scanner.nextLine();
                            String childSurname = stringReceiver("Enter child`s surname: ");
                            int childYearBirthDate = intReceiver("Enter child`s year of birth date: ", "Input again: ");
                            int childMonthBirthDate = intReceiver("Enter child`s month of birth date: ", "Input again: ");
                            int childDayBirthDate = intReceiver("Enter child`s day of birth date: ", "Input again: ");
                            byte childIq = (byte) intReceiver("Enter child`s IQ: ", "Input again: ");

                            Human child;
                            if (childSex.equals("female"))
                                child = getHuman(childName, childSurname, childYearBirthDate, childMonthBirthDate, childDayBirthDate, childIq, true);
                            else {
                                child = getHuman(childName, childSurname, childYearBirthDate, childMonthBirthDate, childDayBirthDate, childIq, false);
                            };
                            try {
                                adoptChild(getFamilyById(familyInd), child);
                                System.out.println("Done\uD83D\uDC4D\n");
                            } catch (FamilyOverflowException familyOverflowException) {
                                System.out.println(familyOverflowException.getMessage() + '\n');
                            }
                        }
                        case 3 -> printMenu();
                        default -> System.out.println("Input again: ");
                    }
                    break;

                case 9:
                    return;
                default:
                    System.out.println("Wrong input...\n");
            }
        }
    }

    private Human getHuman(String name, String surname, int yearBirthDate, int monthBirthDate, int dayBirthDate, byte childIq, boolean isFemale) {
        Human human;
        while (true) {
            try {
                if(isFemale) {
                    human = new Woman(
                            name,
                            surname,
                            dayBirthDate + "/" + monthBirthDate + "/" + yearBirthDate,
                            childIq
                    );
                }
                else human = new Man(
                        name,
                        surname,
                        dayBirthDate + "/" + monthBirthDate + "/" + yearBirthDate,
                        childIq
                );
                break;
            } catch (DateTimeParseException dateTimeParseException) {
                System.out.println("Wrong date. " + dateTimeParseException.getMessage());
                yearBirthDate = intReceiver("Enter year of birth date: ", "Input again: ");
                monthBirthDate = intReceiver("Enter month of birth date: ", "Input again: ");
                dayBirthDate = intReceiver("Enter day of birth date: ", "Input again: ");
            }
        }
        return human;
    }

    String stringReceiver(String message) {
        System.out.print(message);
        return scanner.next();
    }
    int intReceiver(String message, String messageFallback) {
        System.out.print(message);
        while (!scanner.hasNextInt()) {
            System.out.print(messageFallback);
            scanner.next();
        }
        return scanner.nextInt();
    }
}
