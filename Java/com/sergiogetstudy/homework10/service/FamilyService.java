package com.sergiogetstudy.homework10.service;

import com.sergiogetstudy.homework10.dao.FamilyDao;
import com.sergiogetstudy.homework10.domain.family.Family;
import com.sergiogetstudy.homework10.domain.human.Human;
import com.sergiogetstudy.homework10.domain.human.woman.Woman;
import com.sergiogetstudy.homework10.domain.pet.Pet;

import java.time.LocalDate;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalField;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FamilyService {
    private FamilyDao familyDao;

    public void setFamilyDao(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }
    public String displayAllFamilies() {
        String result = Stream
            .iterate(0, i -> i + 1)
            .limit(familyDao.getAllFamilies().size())
            .map(String::valueOf)
            .reduce("", (acc, i) -> acc + i + " - " + familyDao.getAllFamilies().get(Integer.parseInt(i)) + "\n");
        System.out.println(result);

        return result;
    }
    public List<Family> getFamiliesBiggerThan(int count) {
        return familyDao.getAllFamilies()
                .stream()
                .filter(family -> family.countFamily() > count)
                .peek(System.out::println)
                .toList();
    }
    public List<Family>  getFamiliesLessThan(int count) {
        return familyDao.getAllFamilies()
                .stream()
                .filter(family -> family.countFamily() < count)
                .peek(System.out::println)
                .toList();
    }

    public int countFamiliesWithMemberNumber(int memberNumber) {
        return (int) familyDao.getAllFamilies()
                .stream()
                .filter(family -> family.countFamily() == memberNumber)
                .count();
    }
    public Family createNewFamily(Human woman, Human man) {
        if(woman == null || man == null) return null;
        Family family = new Family(woman, man);
        familyDao.saveFamily(family);
        return family;
    }
    public boolean deleteFamilyByIndex(int index) {
        return familyDao.deleteFamily(index);
    }
    public Family bornChild(Family family, String childMansName, String childWomansName) {
        if(family == null) return null;
        Human child = ((Woman) family.getMother()).bornChild(childMansName, childWomansName);
        family.addChild(child);
        if (familyDao.getAllFamilies().contains(family)) familyDao.saveFamily(family);

        return family;
    }
    public Family adoptChild(Family family, Human child) {
        if(family == null) return null;

        family.addChild(child);
        if (familyDao.getAllFamilies().contains(family)) familyDao.saveFamily(family);

        return family;
    }
    public boolean deleteAllChildrenOlderThan(int childAge) {

        boolean isToEdit = familyDao.getAllFamilies()
                .stream()
                .anyMatch(family -> family.getChildren()
                        .stream()
                        .anyMatch(child -> child.getBirthDate() < LocalDate.now().minusYears(childAge).toEpochDay())
                );

        if(isToEdit) {
            familyDao.getAllFamilies()
                .forEach(family -> {
                    family.setChildren(family.getChildren()
                            .stream()
                            .filter(child -> child.getBirthDate() > LocalDate.now().minusYears(childAge).toEpochDay())
                            .toList());
                    familyDao.saveFamily(family);
                });
        }

        return isToEdit;
    }
    public int count() {
        return familyDao.getAllFamilies().size();
    }
    public Family getFamilyById(int index) {
        return familyDao.getFamilyByIndex(index);
    }
    public Set<Pet> getPets(int index) {
        if(familyDao.getFamilyByIndex(index) == null) return null;
        return familyDao.getFamilyByIndex(index).getPets();
    }
    public boolean addPet(int index, Pet pet) {
        if(pet == null) return false;

        Family family = familyDao.getFamilyByIndex(index);

        if(family.getPets() == null) {
            family.setPets(new HashSet<>());
        };

        return family.getPets().add(pet);
    }
}
