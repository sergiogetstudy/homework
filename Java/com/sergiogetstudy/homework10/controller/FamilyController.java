package com.sergiogetstudy.homework10.controller;

import com.sergiogetstudy.homework10.domain.family.Family;
import com.sergiogetstudy.homework10.domain.human.Human;
import com.sergiogetstudy.homework10.domain.pet.Pet;
import com.sergiogetstudy.homework10.service.FamilyService;

import java.util.List;
import java.util.Set;

public class FamilyController {
    FamilyService familyService;

    public void setFamilyService(FamilyService familyService) {
        this.familyService = familyService;
    }

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }
    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }
    public List<Family> getFamiliesBiggerThan(int count) {
        return familyService.getFamiliesBiggerThan(count);
    }
    public List<Family>  getFamiliesLessThan(int count) {
        return familyService.getFamiliesLessThan(count);
    }
    public int countFamiliesWithMemberNumber(int memberNumber) {
        return familyService.countFamiliesWithMemberNumber(memberNumber);
    }
    public Family createNewFamily(Human woman, Human man) {
        return familyService.createNewFamily(woman, man);
    }
    public boolean deleteFamilyByIndex(int index) {
        return familyService.deleteFamilyByIndex(index);
    }
    public Family bornChild(Family family, String childMansName, String childWomansName) {
        return familyService.bornChild(family, childMansName, childWomansName);
    }
    public Family adoptChild(Family family, Human child) {
        return familyService.adoptChild(family, child);
    }
    public boolean deleteAllChildrenOlderThen(int childAge) {
        return familyService.deleteAllChildrenOlderThan(childAge);
    }
    public int count() {
        return familyService.count();
    }
    public Family getFamilyById(int index) {
        return familyService.getFamilyById(index);
    }
    public Set<Pet> getPets(int index) {
        return familyService.getPets(index);
    }
    public boolean addPet(int index, Pet pet) {
        return familyService.addPet(index, pet);
    }
}
