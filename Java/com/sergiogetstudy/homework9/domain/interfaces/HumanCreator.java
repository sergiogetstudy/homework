package com.sergiogetstudy.homework9.domain.interfaces;

import com.sergiogetstudy.homework9.domain.human.Human;

public interface HumanCreator {
    Human bornChild(String childMansName, String childWomansName);
}
