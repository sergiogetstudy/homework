package com.sergiogetstudy.homework9.domain.human.man;

import com.sergiogetstudy.homework9.domain.family.Family;
import com.sergiogetstudy.homework9.domain.human.Human;
import com.sergiogetstudy.homework9.domain.pet.Pet;

import java.util.Map;
import java.util.Set;

public final class Man extends Human {
    public Man(String name, String surname, long birthDate) {
        super(name, surname, birthDate);
    }

    public Man(String name, String surname, long birthDate, Human mother, Human father) {
        super(name, surname, birthDate, mother, father);
    }

    public Man(String name, String surname, String birthDate, byte iq) {
        super(name, surname, birthDate, iq);
    }

    public Man(String name, String surname, long birthDate, byte iq, Map<String, String> schedule, Family family, Set<Pet> pet, Human mother, Human father) {
        super(name, surname, birthDate, iq, schedule, family, pet, mother, father);
    }

    public void repairCar() {
        System.out.println("Let's repair the car!");
    }

    @Override
    public void greetPet(Pet pet) {
        System.out.println("Hey, " + pet.getNickname());
    }
}
