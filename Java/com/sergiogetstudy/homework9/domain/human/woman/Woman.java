package com.sergiogetstudy.homework9.domain.human.woman;

import com.sergiogetstudy.homework9.domain.family.Family;
import com.sergiogetstudy.homework9.domain.human.Human;
import com.sergiogetstudy.homework9.domain.human.man.Man;
import com.sergiogetstudy.homework9.domain.interfaces.HumanCreator;
import com.sergiogetstudy.homework9.domain.pet.Pet;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public final class Woman extends Human implements HumanCreator {

    public Woman(String name, String surname, long birthDate) {
        super(name, surname, birthDate);
    }

    public Woman(String name, String surname, long birthDate, Human mother, Human father) {
        super(name, surname, birthDate, mother, father);
    }

    public Woman(String name, String surname, String birthDate, byte iq) {
        super(name, surname, birthDate, iq);
    }

    public Woman(String name, String surname, long birthDate, byte iq, Map<String, String> schedule, Family family, Set<Pet> pet, Human mother, Human father) {
        super(name, surname, birthDate, iq, schedule, family, pet, mother, father);
    }

    public void makeup() {
        System.out.println("Let's makeup!");
    }

    @Override
    public void greetPet(Pet pet) {
        System.out.println("Hey, " + pet.getNickname() + ". How are you?");
    }

    @Override
    public Human bornChild(String childMansName, String childWomansName) {
        boolean isBoy = new Random().nextBoolean();

        if (isBoy) {
            return new Man(
                    childMansName,
                    getFamily().getFather().getSurname() == null ? null : getFamily().getFather().getSurname(),
                    0,
                    (byte) (((getFamily().getFather().getIq() == 0 || getIq() == 0)) ? 0 :
                            ((getFamily().getFather().getIq() + getIq())/2)),
                    new HashMap<>(),
                    getFamily(),
                    getFamily().getPets(),
                    getFamily().getMother(),
                    getFamily().getFather()
            );
        } else {
            return new Woman(
                    childWomansName,
                    getFamily().getFather().getSurname() == null ? null : getFamily().getFather().getSurname(),
                    0,
                    (byte) (((getFamily().getFather().getIq() == 0 || getIq() == 0)) ? 0 :
                            ((getFamily().getFather().getIq() + getIq())/2)),
                    new HashMap<>(),
                    getFamily(),
                    getFamily().getPets(),
                    getFamily().getMother(),
                    getFamily().getFather()
            );
        }
    }
}
