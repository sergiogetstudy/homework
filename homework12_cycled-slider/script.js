document.addEventListener("DOMContentLoaded", function() {
    const images = document.querySelectorAll(".image-to-show");
    const buttons = document.querySelector(".buttons");

    let currentImg = 0;

    let timerId;

    function createInterval() {
        timerId = setInterval(function(){
            images[currentImg].classList.add("hidden");
            currentImg++;
        
            if (currentImg === images.length) {
                currentImg = 0;
            }
            images[currentImg].classList.remove("hidden");
        }, 3000);
    }

    buttons.addEventListener("click", function(event) {
        console.log(event.target.id);
        if(event.target.id === "pause") {
            clearInterval(timerId);
        } else if (event.target.id === "resume") {
            createInterval();
        }
    })

    createInterval();
})