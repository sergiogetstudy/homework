const ourServicesTabs = document.querySelector('.our-services-section-list');

ourServicesTabs.addEventListener('click', function(event){
    document.querySelector('.our-services-section-list-item-active').classList.remove("our-services-section-list-item-active");

    const currentItem = event.target.closest('.our-services-section-list-item');
    
    currentItem.classList.add
    ("our-services-section-list-item-active");

    const id = currentItem.innerHTML;

    const tabContentItems = document.querySelectorAll('.our-services-article');

    for (let contentItem of tabContentItems){
        contentItem.dataset.descriptionItem === id ? contentItem.classList.remove("not-displayed") : contentItem.classList.add("not-displayed");
    }
});

const moreWorksBtn = document.querySelector("#load-more-works"); 

moreWorksBtn.addEventListener("click", function(){
    notDisplayedImages = document.querySelectorAll(".our-amazing-work-section-gallery > .not-displayed");
    console.log(notDisplayedImages);
    for(let notDispImg of notDisplayedImages){
        notDispImg.classList.remove("not-displayed");
        console.log(notDispImg.classList);
    }
    moreWorksBtn.classList.add("not-displayed");
});

const workFilter = document.querySelector(".our-amazing-work-section-list");

workFilter.addEventListener("click", function(event){
    const currentItem = event.target.closest(".our-amazing-work-section-list-item");
    
    const id = currentItem.innerHTML;

    const images = document.querySelectorAll('.our-amazing-work-section-gallery > img');
    
    for (let img of images){
        if(img.dataset.filter)
        id === img.dataset.filter || id === "All" ? img.classList.remove("filtered") : img.classList.add("filtered");
    }
});

// let i = 1;
// for(let li of carousel.querySelectorAll('li')) {
//   li.style.position = 'relative';
//   li.insertAdjacentHTML('beforeend', `<span style="position:absolute;left:0;top:0">${i}</span>`);
//   i++;
// }

let width = document.querySelector(".people-carousel-list-item").offsetWidth;

let count = 1;

let list = document.querySelector('.people-carousel-list');
let listElems = document.querySelectorAll('.people-carousel-list-item');

let position = 0;

document.querySelector('.prev').onclick = function() {
  position += width * count;

  position = Math.min(position, 0);
  list.style.marginLeft = position + 'px';
};

document.querySelector('.next').onclick = function() {
  position -= width;

  position = Math.max(position, -width);
  list.style.marginLeft = position + 'px';
};