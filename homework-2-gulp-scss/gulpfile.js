"use strict"

const { task } = require("gulp");
const gulp = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const autoprefixer = require('gulp-autoprefixer');
const concat = require("gulp-concat");
const cleanCSS = require("gulp-clean-css");
const uglify = require('gulp-uglify');
const clean = require('gulp-clean');
const browserSync = require('browser-sync').create();

function styles() {
    return gulp.src("./src/scss/**/*.scss")
               .pipe(sass().on('error', sass.logError))
               .pipe(concat("styles.min.css"))
               .pipe(autoprefixer({
                    browsers: ["last 2 versions"],
                    cascade: false
                }))
               .pipe(cleanCSS({level: 2}))
               .pipe(gulp.dest("./dist/"))
               .pipe(browserSync.stream());
}

function scripts() {
    return gulp.src("./src/js/**/*.js")
               .pipe(concat("scripts.min.js"))
               .pipe(uglify({
                   toplevel: true
               }))
               .pipe(gulp.dest("./dist/"));
}

function watch() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    gulp.watch("./src/scss/**/*.scss", styles);
    gulp.watch("./src/js/**/*.js", scripts);
    gulp.watch("./*.html", browserSync.reload);
}

function deleteAll() {
    return gulp.src("./dist/**/*", {read: false})
        .pipe(clean());
}

gulp.task("styles", styles);
gulp.task("scripts", scripts);
gulp.task("watch", watch);
gulp.task('clean', deleteAll);

gulp.task("build", gulp.series(deleteAll, gulp.parallel(styles, scripts)));

gulp.task("dev", gulp.series("build", "watch"));

// const sass = require("gulp-sass")(require("sass"));
// const autoprefixer = require('gulp-autoprefixer');
// const browserSync = require("browser-sync");

// gulp.task("server", function() {
//     browserSync.init({
//         server: {
//             baseDir: "./"
//         }
//     });
// });

// gulp.task("sass", function() {
//     return gulp.src("./src/scss/*.scss")
//                .pipe(sass().on('error', sass.logError))
//                .pipe(autoprefixer({
//                     browsers: ["last 2 versions"],
//                     cascade: false
//                 }))
//                .pipe(gulp.dest("./dist"))
//                .on("end", browserSync.reload);
// });

// gulp.task("watch", function() {
//     gulp.watch("./src/**/*.scss", gulp.series("sass"));
// });

// gulp.task("default", gulp.series(gulp.parallel("sass"), gulp.parallel("watch", "server")));