/*DOM-дерево - це об'єктна модель документа, яку браузер формує ("парсить") з вхідного HTML-документа. На основі даної об'єктної можелі браузер здатен відмалювати ("відрендерити") сторінку на екрані. В такому дереві кожен елемент коду представлений у вигляді об'єтка з всіма випливаючими властивостями (в тому числі і доступність об'єктів JavaSctipt).*/

"use strict"

const inputElements = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

const outputHtml = getHtmlList(inputElements);

showList(outputHtml);
showCouter(3, 0);
setTimeout(clearPage, 3000);



function getHtmlList(contentArray){
   
    const outputElement = contentArray.map(function(inputElem){
        if(Array.isArray(inputElem)){
            return getHtmlList(inputElem);
        } else {
            return `<li>${inputElem}</li>`
        }
    }).join("");
    return `<ul>${outputElement}</ul>`;
}

function showList(htmlArray, parent = document.body){
    parent.insertAdjacentHTML('beforeend', htmlArray);
}

function showCouter(from, to) {
    let div = document.createElement("div");
    document.body.append(div);
    let current = from;
    
    setTimeout(function go() {
        div.innerHTML = `The page will be cleared after ${current} seconds!`;
        if (current > to) {
            setTimeout(go, 1000);
        }
        current--;
    }, 0);
}

function clearPage(){
    document.body.innerHTML = "";
}
