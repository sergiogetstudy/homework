"use strict"

function createNewUser() {

    const newUser = {};
    
    newUser.firstName = prompt("Enter first name");
    newUser.lastName = prompt("Enter last name");

    newUser.getLogin = function () {
        const userLogin = this.firstName[0] + this.lastName;
        return userLogin.toLowerCase();
    };

    newUser.setFirstName = function(newFirstName) {
        this.firstName = newFirstName;
    };
    
    newUser.setLastName = function(newLastName) {
        this.lastName = newLastName;
    };

    return newUser;
}

const user = createNewUser();

console.log(user.getLogin());