// Екранування - це засіб виводу спецсимволів напр. на екран. В JS такий вивід можливий за допомогою т. з. символу екранування - зворотнього слешу "\" та додавання до нього потріного символу. Справа в тому, ще JS використовує спецсимволи в синтаксисі коду, тому звичайне їх написання призведе до порушення синтаксису. Також зворотній слеш дозволяє виводити символи за допомогою кодування в Юнікоді.

"use strict"

function createNewUser() {

    const newUser = {};
    
    newUser.firstName = prompt("Enter first name");
    newUser.lastName = prompt("Enter last name");
    newUser.birthDate = new Date(prompt("Enter you birth datr in dd.mm.yyyy format"));
    
    newUser.getLogin = function () {
        const userLogin = this.firstName[0] + this.lastName;
        return userLogin.toLowerCase();
    };

    newUser.getAge = function () {
        const currentDate = new Date();
        const userAge = parseInt((currentDate - newUser.birthDate)/(365.25 * 24 * 60 * 60 * 1000));
        return userAge;
    }

    newUser.getPassword = function () {
        const userPathword = this.firstName[0].toLocaleUpperCase() + this.lastName.toLowerCase() + this.birthDate.getFullYear();
        return userPathword;
    }

    return newUser;
}

const user = createNewUser();

console.log(user);
console.log("User's login: " + user.getLogin());
console.log("User's age: " + user.getAge());
console.log("User's password: " + user.getPassword());