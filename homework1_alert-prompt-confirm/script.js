/*1. Відмінністю let і const є те, що змінна оголошена як let може бути перезаписана, а const - ні.
Відмінностями let (і const) від var є: 
 - область видимості - var доступні по по всьому коду, тоді як let - тільки в блоці { };
 - let можна присвоювати тільки після оголошення, var - будь-де (це називається всплиття змінної, коли всі оголошені var перемінаються інтерпретатором нагору);
 - при використанні в циклі let на кожній ітерації створюється нова змінна, var же перезаписує ту ж саму змінну.

 2. Використання var є поганим тоном виходячи із самих властивостей var:
  - порушується послідовність коду - змінну можна оголосити після присвоювання їй значення;
  - порушується принцип інкапсуляції - зміння var "видна" в будь-якій частині коду.
*/

"use strict"

let userName;
let userAge;

do
{
    userName = prompt(`What is your name?`, userName);
    userAge = prompt(`What is your аge?`, userAge);
}
while(!userName || !Number(userAge));

if (userAge < 18) 
{
    alert(`You are not allowed to visit this website`);
}
else if (userAge > 22)
{
    alert(`Welcome, ${userName}`);
}
else
{
    if(confirm(`Are you sure you want to continue?`))
    {
        alert(`Welcome, ${userName}`);
    }
    else
    {
        alert(`You are not allowed to visit this website`);
    }
}