import './App.scss';
import Button from "./components/Button.jsx"
import Modal from "./components/Modal.jsx"
import React from 'react';
import modalWindowDeclarations from "./configs/modalConfigs.json";

class App extends React.Component {
  constructor(props) {
    
    super(props);
    this.state = {
      modalShown: null,
    }
  }
  
  handleClose = () => {
    this.setState({modalShown: null});
  }

  renderModal = (event) => {
    const modalId = event.target.dataset.modalId;
    this.setState({
      modalShown: modalWindowDeclarations[modalId - 1],
    })
  }

  render() {
    const actions = (
      <div>
          <button className="modal__button" onClick={() => {this.handleClose()}}>Ok</button>
          <button className="modal__button" onClick={() => {this.handleClose()}}>Cancel</button>
      </div>
      )

    return (
      <>
        <Button 
          modalId="1"
          backgroundColor="#63d437"
          className="button"
          text="Open first modal" 
          onClick={this.renderModal} 
        />
        <Button 
          modalId="2"
          backgroundColor="#d44637"
          className="button"
          text="Open second modal" 
          onClick={this.renderModal} 
        />
        {!(this.state.modalShown === null) && <Modal modalConfig={this.state.modalShown} onClose={this.handleClose} actions={actions} />}
      </>
    );
  }
}

export default App;
