import React from "react";
import "./Modal.scss"

class Modal extends React.Component {
    handleOk = () => { 
        console.log("clicked");
        this.props.onClose() }
        
    handleCancel = () => { this.props.onClose() }

    render() {
        const { className, header, text, closeButton } = this.props.modalConfig;

        return (
            <div className={"modal " + className} onClick={() => {this.props.onClose()}}>
                <div className="modal__window" onClick={(event) => event.stopPropagation()}>
                    <div className="modal__header">
                        <div className="modal-window__header-text">{header}</div>
                        {closeButton && <svg onClick={() => {this.props.onClose()}} className="modal-window__close-btn close-btn" width="20" height="20" viewBox="0 0 26 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1 1L23.5 24" stroke="white" strokeWidth="2" />
                            <path d="M1 23L24 1" stroke="white" strokeWidth="2" />
                        </svg>}
                    </div>
                    <p className="modal__content">{text}</p>
                    {this.props.actions}
                </div>
            </div>
        )
    }
}

export default Modal;