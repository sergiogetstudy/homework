import React from "react";
import "./Button.scss";

class Button extends React.Component {

    render() {
        const {modalId, backgroundColor, className,  onClick, text} = this.props;
        return (
            <button data-modal-id={modalId} className={className} style={{backgroundColor}} onClick={onClick}>{text}</button>
        );
    }
}

export default Button;