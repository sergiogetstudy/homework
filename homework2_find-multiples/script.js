/*
Цикли спрощують розробку тим, що дії, які мають виконуватися багато разів можна записати визначеною умовою, а не дублювати код для кожної дії.
*/

"use strict"

let userNumber;

do{
    userNumber = +prompt(`Enter integer number`, userNumber);
} while(userNumber % 1 !== 0);

if(userNumber < 5){
    console.log(`Sorry, no numbers`);
} else {
    for (let i = 5; i <= userNumber; i += 5){
        console.log(i);
    }
}

alert(`Let's find prime numbers from 'm' to 'n'`);


let m;
let n;
let isCorrectNumbers;

do{
    let userNumber1 = +prompt(`Enter first number`);
    let userNumber2 = +prompt(`Enter second number`);

    if(userNumber1 % 1 !== 0 || userNumber2 % 1 !== 0){
        alert(`Sorry, incorrect data, try again`);
        isCorrectNumbers = false;
    } else {
        isCorrectNumbers = true;

        if(userNumber1 < userNumber2){
            m = userNumber1;
            n = userNumber2;
        } else {
            m = userNumber2;
            n = userNumber1; 
        }
    }
} while(!isCorrectNumbers);

for (m; m <= n; m++) {
    let isPrime = true;
    let i = 2;

    while (isPrime && i <= m / 2){
        if (m % i === 0){
            isPrime = false;
        }
        i++;
    }

    if (isPrime){
        console.log(m);
    }
}