/*
1. Функції в програмуванні потрібні для описання деякої логіки поведінки окремої частини програми. Опис такої логіки дозволяє уникнути дублювання коду, адже одна і та ж функція може бути викликана потрібну кількість разів.

2. Передавання аргументів дозволяє використовувати дані параметри у тілі функції. Це дозволяє використовувати одну і ту ж логіку для різних вхідних параметів (звичайно, використовуючи вірний синтаксис) без необхідності створювати нову функцію, маніпулювати вхідними даними, або керувати поведінкою функції відповідно до різних вхідних параметрів.
*/

"use strict"

let userNumbers;
let userOperator;
let isCorrectData;
let numbersArray;

do{
    userNumbers = prompt(`Enter 2 numbers separated by space (Example: 2 2)`, userNumbers);
    userOperator = prompt(`Enter math operator (+, -, * or /)`, userOperator);

    numbersArray = userNumbers.split(` `, 2);
    
    if((userOperator === "+" || userOperator === "-" || userOperator === "*" || userOperator === "/") && validateNumbers(numbersArray)){
        isCorrectData = true;
    }

    function validateNumbers(array) {
        for (let number in array){
            if (!Number(array[number])){
                return false;
            }
        }
        return true;
    }
} while(!isCorrectData);

console.log(calculate(userOperator, numbersArray));

function calculate(operator, nums){
    switch(operator) {
        case '+':
            return +nums[0] + +nums[1];
      
        case '-':
            return nums[0] - nums[1];

        case '*':
            return nums[0] * nums[1];

        case '/':
            return nums[0] / nums[1];   
      }
}