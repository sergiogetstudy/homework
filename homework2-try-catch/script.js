/*
Конструкцію "try...catch" доцільно використовувати:
- коли результат виконання фукнції може дати неочікуваний результат (наприклад якщо розпарсити некоректний JSON);
- коли ми хочемо згенерувати власну помилку в залежності від результату умови, яка нас не влаштовує (як приклад ця робота), та обробити цю помилку блокмом "try...catch";
- коли передбачити всі умови, коли код "впаде", занадто складно
*/

"use strict"

const books = [
  { 
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70 
  }, 
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  }, 
  { 
    name: "Тысячекратная мысль",
    price: 70
  }, 
  { 
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70
  }, 
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  }
];

function createAllPropertiesSet(list = [{"no key": "no value"}]) {
  const allPropetries = new Set();

  for(let element of list) {
    for (let prop in element) {
      allPropetries.add(prop);
    }
  } 
  return allPropetries;
}

function renderList(target = "body", dataArray = [{"no key": "no value"}], propertiesSet = new Set(["no key"])) {

  const newList = document.createElement("ul");
  document.querySelector(target).append(newList);

  let currentIteration = 1;

  dataArray.forEach(listElement => {

    const newListItem = document.createElement("li");
    newList.append(newListItem);

    propertiesSet.forEach(property => {
      try{
        if(!listElement.hasOwnProperty(property)) {
          throw new SyntaxError(`${property} on list item ${currentIteration} is not exist!`);
        }
        newListItem.innerHTML += `${property}: ${listElement[property]} <br/>`;
      }
      catch(err) {
        if(err instanceof SyntaxError) {
          console.log(`Syntax error: ${err.message}`);
        }
        else {
          throw err;
        }
      }
    });
    currentIteration++;
    newListItem.innerHTML += `<br/>`;
  });
}

const allBooksProperties = createAllPropertiesSet(books);

renderList("#root", books, allBooksProperties);

window.onerror = function(message, url, line, col, error) {
  alert(`${message}\n In ${line}: ${col} on ${url}`);
}