class Api {

    static getJSON = url => 
        fetch(url, {
            method: 'GET',
        })
        .then(response => response.json());

    static deleteFromServer = url => 
        fetch(url, {
            method: 'DELETE',
        })

    static addCardToServer = url => 
        fetch(url, {
            method: 'POST',
            body,
            headers: {
            'Content-type': 'application/json'
            }
        })
        .then(response => response.json());

    static changingRequest = (url, obj) => 
        fetch(url, {
            method: 'PUT',
            body: JSON.stringify(obj),
            headers: {
            'Content-Type': 'application/json'
        }
    });

    static pushToServer = (url, obj) =>
        fetch(url, {
            method: 'POST',
            body: JSON.stringify(obj),
            headers: {
              'Content-type': 'application/json'
            }
    });
    
    static wait = (delay = 300) => new Promise((resolve) => setTimeout(resolve, delay));
}
