class usersCollection {
    constructor() {
        this.usersList = [];
    }

    getUsersCollection() {

        const getFromServer = () => Api.getJSON("https://ajax.test-danit.com/api/json/users")
            .then(users => {
                
                users.forEach(user => 
                    {
                        const author = new User(user.name, user.username, user.email, user.id);
                        this.usersList.push(author);
                    });
                });
            
        return new Promise(() => getFromServer());
    }
}