class App {
    initApp() {
        const users = new usersCollection();
        const cards = new cardsCollection();
        cards.getCardsCollection(users);

        document.querySelector("#add-post").addEventListener("click", () => {
            const modal = new Modal();
            
            document.querySelector(".modal__button").addEventListener("click", () => {
                cards.addPost(users);
                modal.closeModalWindow();
            });
        });
    }
}

const app = new App();

Api.wait(1500).then(app.initApp);
