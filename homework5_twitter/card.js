class Card {
    constructor(header, content, id, authorId) {
        this.header = header;
        this.content = content;
        this.id = id;
        this.authorId = authorId;
        this.elementInDOM = null;

        this.callbacks = {
            'delete': [],
        }; 
    }

    renderCard(target) {

        const newListItem = document.createElement("li");
        newListItem.setAttribute("data-id", this.id);
        document.querySelector(target).prepend(newListItem);
        newListItem.classList.add("post");

        this.elementInDOM = newListItem;

        const postHeading = document.createElement("div");
        newListItem.append(postHeading);
        postHeading.setAttribute("data-heading-id", this.id);
        postHeading.classList.add("post__panel")

        const itemTitle = document.createElement("h2");
        itemTitle.innerHTML = this.header;
        itemTitle.classList.add("post__heading")
        newListItem.append(itemTitle);

        const itemContent = document.createElement("p");
        itemContent.innerHTML = this.content;
        newListItem.append(itemContent);
        
        const iconsWrapper = document.createElement("div");
        iconsWrapper.classList.add("post__icon-wrapper");
        postHeading.append(iconsWrapper);

        iconsWrapper.insertAdjacentHTML("afterbegin", 
            `<svg class="post__edit-btn" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" viewBox="0 -256 1850 1850" id="svg3025" version="1.1" inkscape:version="0.48.3.1 r9886" width="24" height="18" sodipodi:docname="edit_font_awesome.svg">
                <metadata id="metadata3035">
                <rdf:RDF>
                    <cc:Work rdf:about="">
                    <dc:format>image/svg+xml</dc:format>
                    <dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage"/>
                    </cc:Work>
                </rdf:RDF>
                </metadata>
                <defs id="defs3033"/>
                <sodipodi:namedview pagecolor="#ffffff" bordercolor="#666666" borderopacity="1" objecttolerance="10" gridtolerance="10" guidetolerance="10" inkscape:pageopacity="0" inkscape:pageshadow="2" inkscape:window-width="640" inkscape:window-height="480" id="namedview3031" showgrid="false" inkscape:zoom="0.13169643" inkscape:cx="896" inkscape:cy="896" inkscape:window-x="0" inkscape:window-y="25" inkscape:window-maximized="0" inkscape:current-layer="svg3025"/>
                <g transform="matrix(1,0,0,-1,30.372881,1373.7966)" id="g3027">
                <path d="M 888,352 1004,468 852,620 736,504 v -56 h 96 v -96 h 56 z m 440,720 q -16,16 -33,-1 L 945,721 q -17,-17 -1,-33 16,-16 33,1 l 350,350 q 17,17 1,33 z m 80,-594 V 288 Q 1408,169 1323.5,84.5 1239,0 1120,0 H 288 Q 169,0 84.5,84.5 0,169 0,288 v 832 Q 0,1239 84.5,1323.5 169,1408 288,1408 h 832 q 63,0 117,-25 15,-7 18,-23 3,-17 -9,-29 l -49,-49 q -14,-14 -32,-8 -23,6 -45,6 H 288 q -66,0 -113,-47 -47,-47 -47,-113 V 288 q 0,-66 47,-113 47,-47 113,-47 h 832 q 66,0 113,47 47,47 47,113 v 126 q 0,13 9,22 l 64,64 q 15,15 35,7 20,-8 20,-29 z M 1312,1216 1600,928 928,256 H 640 v 288 z m 444,-132 -92,-92 -288,288 92,92 q 28,28 68,28 40,0 68,-28 l 152,-152 q 28,-28 28,-68 0,-40 -28,-68 z" id="path3029" inkscape:connector-curvature="0" style="fill:currentColor"/>
                </g>
          </svg>`
        );

        iconsWrapper.insertAdjacentHTML("afterbegin", 
            `<svg class="post__ok-btn post__btn--hidden" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
                <path d="M 20.292969 5.2929688 L 9 16.585938 L 4.7070312 12.292969 L 3.2929688 13.707031 L 9 19.414062 L 21.707031 6.7070312 L 20.292969 5.2929688 z">
                </path>
            </svg>`
        );

        iconsWrapper.insertAdjacentHTML("beforeend", 
            `<svg class="post__close-btn close-btn" width="15" height="15" viewBox="0 0 26 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M1 1L23.5 24" stroke="black" stroke-width="2" />
                <path d="M1 23L24 1" stroke="black" stroke-width="2" />
            </svg>`
        );

        const closeButton = newListItem.querySelector(".post__close-btn");
        closeButton.addEventListener("click", this.removeCard.bind(this));

        const editButton = newListItem.querySelector(".post__edit-btn");
        editButton.addEventListener("click", this.changeCard.bind(this));

        const okBnt = this.elementInDOM.querySelector(".post__ok-btn");
        okBnt.addEventListener("click", () => {
            this.saveChanges();
            
        });
    }

    changeCard() {

        this.elementInDOM.querySelector(".post__edit-btn").classList.toggle("post__btn--hidden");
        this.elementInDOM.querySelector(".post__ok-btn").classList.toggle("post__btn--hidden");
    
        
        this.elementInDOM.querySelector("h2").innerHTML = `<input class="post__edit post__edit-heading" type="text" value="${this.header}">`;
        this.elementInDOM.querySelector("p").innerHTML = `<textarea class="post__edit post__edit-content" wrap="off">${this.content}</textarea>`;

        this.header = this.elementInDOM.querySelector(".post__edit-heading").value;
        
        this.elementInDOM.querySelector(".post__edit-content").addEventListener("change", () => {
            this.content = this.elementInDOM.querySelector(".post__edit-content").value;
            this.elementInDOM.querySelector(".post__edit-content").innerHTML = this.content;
        })
    }
    
    saveChanges() {
        const newHeadingInput = this.elementInDOM.querySelector(".post__edit-heading");
        const newContentInput = this.elementInDOM.querySelector(".post__edit-content");
        
        this.elementInDOM.querySelector(".post__edit-btn").classList.toggle("post__btn--hidden");   
        this.elementInDOM.querySelector(".post__ok-btn").classList.toggle("post__btn--hidden");
        
        const body = {
            id: this.id,
            userId: 1,
            title: newHeadingInput.value,
            body: newContentInput.innerHTML
        }
        
        Api.changingRequest(`https://ajax.test-danit.com/api/json/posts/${this.id}`, body)
        .then((response) => {
            console.log(response);
                this.header = newHeadingInput.value;
                this.content = newContentInput.innerHTML;

                this.elementInDOM.querySelector("h2").innerHTML = newHeadingInput.value;
                this.elementInDOM.querySelector("p").innerHTML = newContentInput.innerHTML;

            });

    }

    addEventListener(eventName, callback) {
        this.callbacks[eventName].push(callback)
    }

    removeCard() {
        this.callbacks.delete.forEach((callback) => {callback()});
    }
}