class cardsCollection {
    constructor() {
        this.cardsList = [];
    }

    getCardsCollection(users) {

        users.getUsersCollection()
            .then(Api.getJSON("https://ajax.test-danit.com/api/json/posts")
            .then((commits) => {
                console.log(commits);
                commits.forEach(commit => 
                    {
                        document.body.classList.add('loaded');
                        
                        const card = new Card(commit.title, commit.body, commit.id, commit.userId);
                        
                        this.cardsList.push(card);
                        
                        card.author = users.usersList[card.authorId - 1];
 
                        card.renderCard(".posts__list");
                        users.usersList.find(user => user.userId === card.authorId).renderUserIntoCard(card.id);

                        card.addEventListener('delete', () => {this.removeById(card.id)});
                    });
                })
            )
    }

    addPost(users) {
        const header = document.querySelector("#input-heading").value;
        const content = document.querySelector("#input-content").value;
        const newPostId = this.cardsList.length + 1;
        const userId = users.usersList.find(user => user.userId === 1).userId;

        const body = {
            body: content,
            id: newPostId,
            title: header,
            userId: userId
        };

        const card = new Card(header, content, newPostId, userId);

        const renderNewPost = () => {
            this.cardsList.push(card);

            card.renderCard(".posts__list");
            users.usersList.find(user => user.userId === card.authorId).renderUserIntoCard(card.id);
            
            card.addEventListener('delete', () => {this.removeById(newPostId)});
        }

        Api.pushToServer(`https://ajax.test-danit.com/api/json/posts`, body)
            .then(renderNewPost);
    }

    removeById(cardId) {
        Api.deleteFromServer(`https://ajax.test-danit.com/api/json/posts/${cardId}`)
        .then(() => {
            const deleteCardFromDom = () => {
                const cardToRemove = this.cardsList.find(card => card.id === cardId)
                cardToRemove.elementInDOM.remove();
            }
            deleteCardFromDom();

            const deleteCardFromList = () => {
                const cardIndex = this.cardsList.findIndex((card) => {
                    return card.id === cardId;
                });
                this.cardsList.splice(cardIndex, 1);
            }
            deleteCardFromList();
        });
    }
}