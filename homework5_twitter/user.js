class User {
    constructor(userFullName, userName, userEmail, userId){
        this.userFullName = userFullName;
        this.userName = userName;
        this.userEmail = userEmail;
        this.userId = userId;
    }
    
    renderUserIntoCard(cardId) {
        const target = document.querySelector(`[data-heading-id = "${cardId}"]`);
        const postAuthorWrapper = document.createElement("div");
        target.prepend(postAuthorWrapper);

        postAuthorWrapper.insertAdjacentHTML("afterbegin", 
            `<span class="post__author-full-name">${this.userFullName}</span>
             <span class="post__author-name">${this.userName}</span>
             <span class="post__author-email">${this.userEmail}</span>`)
    }
}
