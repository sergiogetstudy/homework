class Modal {
    constructor() {
        this.elementInDOM = null;
        this.render();
    }
    render() {
        
        const modalBackground = document.createElement("div");
        modalBackground.classList.add("modal");
        document.querySelector("body").prepend(modalBackground);
        this.elementInDOM = modalBackground;

        const modalWindow = document.createElement("div");
        modalWindow.classList.add("modal__window");
        modalBackground.append(modalWindow);
        
        const modalHeadingWrap = document.createElement("div");
        modalHeadingWrap.classList.add("modal__heading-wrap");
        modalWindow.append(modalHeadingWrap);
        
        const modalHeading = document.createElement("h2");
        modalHeading.classList.add("modal__heading");
        modalHeadingWrap.append(modalHeading);
        
        modalHeadingWrap.insertAdjacentHTML("beforeend", 
        `<svg class="modal__close-btn close-btn" width="15" height="15" viewBox="0 0 26 27" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M1 1L23.5 24" stroke="black" stroke-width="2" />
        <path d="M1 23L24 1" stroke="black" stroke-width="2" />
        </svg>`
        );
        document.querySelector(".modal__close-btn").addEventListener("click", this.closeModalWindow.bind(this));
        
        const renderInput = (id, labelName, inputType, target) => {
            const newLabel = document.createElement("label");
            newLabel.classList.add("modal__label");
            newLabel.setAttribute("for", id);
            newLabel.innerHTML = labelName;
            target.append(newLabel);
            
            const newInput = document.createElement(inputType);
            newInput.classList.add("modal__input");
            newInput.setAttribute("id", id);
            target.append(newInput);
        }
        modalHeading.innerHTML = "Новая публикация";
        renderInput("input-heading", "Название публикации:", "input", modalWindow);
        renderInput("input-content", "Текст публикации:", "textarea", modalWindow);

        const modalSubmitBnt = document.createElement("button");
        modalSubmitBnt.setAttribute("type", "submit");
        modalSubmitBnt.classList.add("add-post-btn");
        modalSubmitBnt.classList.add("modal__button");
        modalSubmitBnt.innerHTML = "Опубликовать";
        modalWindow.append(modalSubmitBnt);
    }

    closeModalWindow() {
        this.elementInDOM.remove();
    }
}