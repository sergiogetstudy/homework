/*Метод forEach(); перебирає елементи масиву. Вхідним параметром методу є т.з. callback-функція:
forEach(callback(){ });
При роботі з масивом вхідним параметорм callback-фукнції є поточний елемент масиву. Отже тіло callback-фукнції буде виконано для кожного елементу масиву.
*/

"use strict"

function getArray(){
    const userArray = ['hello', 'world', 23, '23', null];
    return userArray;
}

function getDataType(){
    const userDataType = prompt(`What type of data do you want to exclude from the array?`).toLowerCase();

    return userDataType;
}

function filterBy(arr, type) {
    const newArray = [];

    arr.forEach(function(element){
        if(typeof(element) !== type){
            newArray.push(element);
        }
    });

    return newArray;
}

const arrayOfAnyData = getArray();
const dataType = getDataType();
const filteredArray = filterBy(arrayOfAnyData, dataType);

console.log(filteredArray);