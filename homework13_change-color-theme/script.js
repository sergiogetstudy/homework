function createAndAppendStyle(tag, atribute, atributeValue) {
    const style = document.createElement(tag);
    style.setAttribute("rel", "stylesheet");
    style.setAttribute(atribute, atributeValue);
    document.head.append(style);
}

let styleLink = localStorage.getItem("href");

if(styleLink === null) {
    createAndAppendStyle("link", "href", "css/style.css");
    localStorage.setItem("href", "css/style.css");
} else {
    createAndAppendStyle("link", "href", styleLink);
}

const changeThemeButton = document.querySelector(".header-button-change-theme");

changeThemeButton.addEventListener("click", function() {
    document.head.lastChild.remove();

    styleLink === "css/style.css" ? styleLink = "css/dark-style.css" : styleLink = "css/style.css";
    
    createAndAppendStyle("link", "href", styleLink);
    localStorage.setItem("href", styleLink);
});