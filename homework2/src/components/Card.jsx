import React from "react";
import Button from "./Button";
import "./Card.scss";
import { FaStar } from "react-icons/fa";
import PropTypes from "prop-types";

export default class Card extends React.Component {
  render() {
    const { name, price, imgPath, id, color } = this.props.data;
    const { handleAddToCard, onChange, favouriteGoods } = this.props;

    let uah = Intl.NumberFormat("ua", {style: "currency", currency: "UAH"});

    return (
      <li className="card">
        <FaStar id={id} onClick={() => onChange(id)} className="card__star" color={favouriteGoods.find(favourite => favourite === id) ? "red" : "grey"} />
        <img className="card__img" src={imgPath} alt={name} height="350px"/>
        <h3 className="card__heading">{name}</h3>
        <span className="card__desc">Артикул: {id}</span>
        <span className="card__desc">Колір: {color}</span>
        <span className="card__desc">Ціна: {typeof price === "number" ? uah.format(price) : price}</span>
        <Button modalId="1" itemId={id} backgroundColor="#1d1d1f" className="card__button" onClick={handleAddToCard} text="Add to cart" />
      </li>
    )
  }
}

Card.propTypes = {
  data: PropTypes.exact({
    name: PropTypes.string.isRequired,
    price: PropTypes.number,
    imgPath: PropTypes.string,
    id: PropTypes.string,
    color: PropTypes.string
  }),
  handleAddToCard: PropTypes.func,
  onChange: PropTypes.func,
  favouriteGoods: PropTypes.array
}

Card.defaultProps = {
  data: {
    name: "<назва смартфону>",
    price: "<не вказана>",
    imgPath: "./imgs/default-phone.png",
    id: "<не вказано>",
    color: "<не вказано>"
  },
}
