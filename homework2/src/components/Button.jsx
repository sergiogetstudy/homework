import React from "react";
import "./Button.scss";
import propTypes from "prop-types";

class Button extends React.Component {

    render() {
        const {modalId, itemId, backgroundColor, className,  onClick, text} = this.props;
        return (
            <button data-modal-id={modalId} data-item-id={itemId} className={className} style={{backgroundColor}} onClick={onClick}>{text}</button>
        );
    }
}

export default Button;

Button.propTypes = {
    modalId: propTypes.string,
    itemId: propTypes.string,
    backgroundColor: propTypes.string,
    className: propTypes.string,
    onClick: propTypes.func,
    text: propTypes.string
}

Button.defaultProps = {
    modalId: "1",
    itemId: "",
    backgroundColor: "black",
    className: "card__button",
    text: "Add to cart"
}