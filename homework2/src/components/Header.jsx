import React from "react";
import { FaShoppingBag, FaShoppingCart } from "react-icons/fa";
import "./Header.scss";

export default class Header extends React.Component {
  render() {
    return (
      <div className="header">
        <div className="header__logo">
          <FaShoppingBag size={32} fill="white" className="header__logo-icon" />
          <FaShoppingBag size={32} fill="white" className="header__logo-icon--hover" />
          <h1 className="header__logo-name">Shopper</h1>
        </div>
        <FaShoppingCart size={26} />
      </div>
    )
  }
}