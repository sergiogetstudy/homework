import React from "react";
import Card from "./Card";
import "./GoodsSection.scss";
import PropTypes from 'prop-types';

export default class GoodsSection extends React.Component {
  handleChange = (id) => {
    this.props.onFavourite(id);
  }
  
  render() {
    const {goods, favouriteGoods, handleAddToCard} = this.props;

    return (
      <div className="goods-section">
        <h2 className="goods-section__heading">Смартфони</h2>
        <ul className="goods-section__list">
          {goods.map(item => (
            <Card 
              key={item.id} 
              data={item} 
              favouriteGoods={favouriteGoods} 
              handleAddToCard={handleAddToCard} 
              onChange={() => this.handleChange(item.id)} 
            />))}
        </ul>
      </div>
    )
  }
}

GoodsSection.propTypes = {
  goods: PropTypes.array,
  favouriteGoods: PropTypes.array,
  handleAddToCard: PropTypes.func
}

GoodsSection.defaultProps = {
  goods: [
    {
      name: "<назва смартфону>",
      price: "<не вказана>",
      imgPath: "./imgs/default-phone.png",
      id: "<не вказано>",
      color: "<не вказано>"
    }
  ]
}