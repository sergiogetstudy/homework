import './App.scss';
import Modal from "./components/Modal.jsx"
import React from 'react';
import modalWindowDeclarations from "./configs/modalConfigs.json";
import GoodsSection from './components/GoodsSection';
import Header from './components/Header';

class App extends React.Component {
  constructor(props) {
    
    super(props);
    this.state = {
      goodsList: [{}],
      isLoaded: false,
      error: null,
      goodsOnCart: [],
      favouriteGoods: [],
      modalShown: null,
      itemToCart: null,
    }
  }

  componentDidMount() {
    fetch("./configs/smartphones.json")
      .then(response => response.json())
      .then(data => {this.setState({
        goodsList: data,
        isLoaded: true
      });
    },
      (error) => {this.setState({ isLoaded: true, error })}
    );
  }

  componentDidUpdate(prevProps, prevState) {
    if(this.state.goodsOnCart !== prevState.goodsOnCart) {
      localStorage.setItem('goodsOnCart', this.state.goodsOnCart);
    }
    if(this.state.favouriteGoods !== prevState.favouriteGoods) {
      localStorage.setItem('favouriteGoods', this.state.favouriteGoods);
    }
  }

  handleAddToCart = () => {
    this.setState({goodsOnCart: 
      [...this.state.goodsOnCart, this.state.itemToCart],
      itemToCart: null
    });
    this.handleClose();
  }

  renderModal = (e) => {
    const modalId = e.target.dataset.modalId;
    
    this.setState({
      modalShown: modalWindowDeclarations[modalId - 1],
      itemToCart: e.target.dataset.itemId,

    })
  }

  handleClose = () => {
    this.setState({
      modalShown: null, 
      itemToCart: null
    });
  }

  toggleToFavourite = (id) => {
    const favouritesList = this.state.favouriteGoods;
    const isChecked = favouritesList.find(favourite => favourite === id);

    if(isChecked) {
      const newFavouriteList = favouritesList.filter(favourite => favourite !== id);
      this.setState({favouriteGoods: newFavouriteList});
    } else {
      this.setState({favouriteGoods: [...favouritesList, id]});
    }
  }

  render() {
    const { error, isLoaded, goodsList, favouriteGoods } = this.state;

    const actions = (
      <div>
          <button 
            className="modal__button" 
            onClick={() => {this.handleAddToCart()}}
          >Ok</button>
          <button 
            className="modal__button" 
            onClick={() => {this.handleClose()}}
          >Cancel</button>
      </div>
    )

    if(error) {
      return <div>Помилка: {error.massage}</div>
    } else if(!isLoaded) {
      return <div>Завантаження...</div>
    } else {
      return (
        <>
          <Header />
          <GoodsSection 
            goods={goodsList} 
            favouriteGoods={favouriteGoods} 
            handleAddToCard={this.renderModal} 
            onFavourite={this.toggleToFavourite} 
          />
          {!(this.state.modalShown === null) && 
            <Modal 
              modalConfig={this.state.modalShown} 
              onClose={this.handleClose} 
              actions={actions} 
            />}
        </>
        );
    }
  }
}

export default App;
