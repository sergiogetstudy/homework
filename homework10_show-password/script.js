const formElement = document.querySelector(".password-form");

formElement.addEventListener("click", eventHandler);

function eventHandler(event) {

    let passwordInputs = document.querySelectorAll(".password");

    if(event.target.closest(".icon-password")){
        let eyeIcons = document.querySelectorAll(".icon-password");

        for(let icon of eyeIcons) {
            if (icon.classList.contains("fa-eye-slash")) {
                icon.classList.remove("fa-eye-slash");
                icon.classList.add("fa-eye");
                
                for (let password of passwordInputs) {
                    password.type = "text";
                }
            } else {
                icon.classList.remove("fa-eye");
                icon.classList.add("fa-eye-slash");

                for (let password of passwordInputs) {
                    password.type = "password";
                }
            }
        }
    }

    function validatePasswords() {
        if(passwordInputs[0].value === passwordInputs[1].value && !!passwordInputs[0].value && !!passwordInputs[0].value){
            return true;
        }
    }

    if(event.target.type === "submit"){
        if(validatePasswords()) {
            if( document.querySelector("#error-message") ) {
                document.querySelector("#error-message").remove();
            }
            alert("You are welcome");
        } else {
            event.preventDefault(); //preventDefault() потрібен для припинення подальших подій, у тому числі "submit"
            passwordInputs[1].insertAdjacentHTML("afterend", 
            '<div id="error-message" style="color:red">Нужно ввести одинаковые значения</div>');
        }
    }
}